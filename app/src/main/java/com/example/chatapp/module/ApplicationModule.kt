package com.example.chatapp.module

import com.example.chatapp.repository.ChatListRepo
import com.example.chatapp.repository.MessageRepo
import com.example.chatapp.repository.UsersRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object RepositoryModule {
    @Provides
    @ViewModelScoped
    fun provideChatListRepository(): ChatListRepo {
        return ChatListRepo()
    }

    @Provides
    @ViewModelScoped
    fun provideUserListRepository(): UsersRepo {
        return UsersRepo()
    }

    @Provides
    @ViewModelScoped
    fun provideMessageRepository(): MessageRepo {
        return MessageRepo()
    }

}