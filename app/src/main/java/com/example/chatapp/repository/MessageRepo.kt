package com.example.chatapp.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.chatapp.model.Messages
import com.example.chatapp.utils.Utils
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query

class MessageRepo {

    private val db = FirebaseFirestore.getInstance()

    fun getMessage(userId: String): LiveData<List<Messages>> {
        val messages = MutableLiveData<List<Messages>>()

        val uniqueId = listOf(Utils.getUidLoggedIn(), userId).sorted()
        uniqueId.joinToString("")

        db.collection("Messages").document(uniqueId.toString()).collection("chats")
            .orderBy("time", Query.Direction.DESCENDING).limit(30)
            .addSnapshotListener { value, error ->
                if (error != null) {
                    return@addSnapshotListener
                }

                val messageList = mutableListOf<Messages>()

                if (!value!!.isEmpty) {
                    value.documents.forEach {
                        val messageModal = it.toObject(Messages::class.java)

                        if (messageModal!!.fromId == Utils.getUidLoggedIn() && messageModal.toId == userId ||
                            messageModal.fromId == userId && messageModal.toId == Utils.getUidLoggedIn()
                        ) {

                            messageModal.let { messages1 ->
                                messageList.add(messages1)
                            }
                        }
                    }
                    messages.value = messageList
                }
            }
        return messages
    }
}