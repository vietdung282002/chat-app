package com.example.chatapp.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.chatapp.model.RecentChat
import com.example.chatapp.model.Users
import com.example.chatapp.utils.Utils
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query

class ChatListRepo {
    private val db = FirebaseFirestore.getInstance()

    fun getAllChatList(): LiveData<LinkedHashMap<RecentChat,Users?>> {
        val mainChatList = MutableLiveData<LinkedHashMap<RecentChat,Users?>>()

        db.collection("ChatRoom").document(Utils.getUidLoggedIn()).collection("userId")
            .orderBy("time", Query.Direction.DESCENDING).addSnapshotListener { value, error ->
                if(error!=null){
                    return@addSnapshotListener
                }
                val chatList = LinkedHashMap<RecentChat,Users?>()
                value?.forEach{
                    val recentModal = it.toObject(RecentChat::class.java)
                    recentModal.let {recent ->
                        chatList[recent] = null
                        db.collection("Users").document(recent.userId)
                            .addSnapshotListener { value, _ ->
                                if (value!!.exists()) {
                                    val user = value.toObject(Users::class.java)
                                    if (user != null) {

                                        chatList[recent] = user
                                    }
                                }
                                mainChatList.value = chatList
                            }
                    }
                }

            }
        return mainChatList
    }

}