package com.example.chatapp.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.chatapp.model.Users
import com.example.chatapp.utils.Utils
import com.example.chatapp.utils.Utils.Companion.FRIEND
import com.example.chatapp.utils.Utils.Companion.FRIEND_REQUEST
import com.example.chatapp.utils.Utils.Companion.NOT_RELATION
import com.example.chatapp.utils.Utils.Companion.REQUESTED_FRIEND
import com.example.chatapp.utils.Utils.Companion.normalizeName
import com.google.firebase.firestore.FirebaseFirestore

class UsersRepo {
    private var db: FirebaseFirestore = FirebaseFirestore.getInstance()

    fun getUserById(userId: String): LiveData<Users> {
        val users = MutableLiveData<Users>()

        db.collection("Users").addSnapshotListener { snapshot, exception ->
            if (exception != null) {
                return@addSnapshotListener
            }

            snapshot?.documents?.forEach { document ->
                val user = document.toObject(Users::class.java)

                if (user!!.userId != Utils.getUidLoggedIn() && user.userId == userId) {
                    user.let {
                        users.value = it
                    }
                }

            }
        }
        return users
    }


    fun getAllUser(
        friendLists: List<String>?,
        friendRequested: List<String>?,
        friendRequests: List<String>?
    ): LiveData<List<Users>> {
        val users = MutableLiveData<List<Users>>()
        db.collection("Users").addSnapshotListener { snapshot, exception ->
            if (exception != null) {
                return@addSnapshotListener
            }

            val usersList = mutableListOf<Users>()
            snapshot?.documents?.forEach { document ->
                val user = document.toObject(Users::class.java)

                if (user!!.userId != Utils.getUidLoggedIn()) {

                    user.let {
                        it.relation = NOT_RELATION
                        if (friendLists != null) {
                            if (it.userId in friendLists) {
                                it.relation = FRIEND
                            }
                        }
                        if (friendRequests != null) {
                            if (it.userId in friendRequests) {
                                it.relation = FRIEND_REQUEST
                            }
                        }
                        if (friendRequested != null) {
                            if (it.userId in friendRequested) {
                                it.relation = REQUESTED_FRIEND
                            }
                        }

                        usersList.add(it)
                    }
                }
                users.value = usersList
            }
        }
        return users
    }

    fun getFriendUsers(friendList: List<String>?): LiveData<List<Users>> {
        val users = MutableLiveData<List<Users>>()

        db.collection("Users").addSnapshotListener { snapshot, exception ->
            if (exception != null) {
                return@addSnapshotListener
            }

            val usersList = mutableListOf<Users>()
            snapshot?.documents?.forEach { document ->
                val user = document.toObject(Users::class.java)

                if (friendList != null) {
                    if (user!!.userId != Utils.getUidLoggedIn() && user.userId in friendList) {

                        user.let {
                            it.relation = FRIEND
                            usersList.add(it)
                        }
                    }
                }
                users.value = usersList
            }
        }
        return users
    }

    fun getFriendRequestsUsers(friendRequestsList: List<String>): LiveData<List<Users>> {
        val users = MutableLiveData<List<Users>>()

        db.collection("Users").addSnapshotListener { snapshot, exception ->
            if (exception != null) {
                return@addSnapshotListener
            }

            val usersList = mutableListOf<Users>()
            snapshot?.documents?.forEach { document ->
                val user = document.toObject(Users::class.java)

                if (user!!.userId != Utils.getUidLoggedIn() && user.userId in friendRequestsList) {

                    user.let {
                        it.relation = FRIEND_REQUEST
                        usersList.add(it)
                    }
                }
                users.value = usersList
            }
        }
        return users
    }

    fun getRequestedFriendUsers(requestedFriend: List<String>): LiveData<List<Users>> {
        val users = MutableLiveData<List<Users>>()

        db.collection("Users").addSnapshotListener { snapshot, exception ->
            if (exception != null) {
                return@addSnapshotListener
            }

            val usersList = mutableListOf<Users>()
            snapshot?.documents?.forEach { document ->
                val user = document.toObject(Users::class.java)

                if (user!!.userId != Utils.getUidLoggedIn() && user.userId in requestedFriend) {

                    user.let {
                        it.relation = REQUESTED_FRIEND
                        usersList.add(it)
                    }
                }
                users.value = usersList
            }
        }
        return users
    }

    fun getSearchUser(searchQuery: String, friendList: List<String>?): LiveData<List<Users>> {
        val users = MutableLiveData<List<Users>>()

        db.collection("Users").addSnapshotListener { snapshot, exception ->
            if (exception != null) {
                return@addSnapshotListener
            }

            val usersList = mutableListOf<Users>()
            snapshot?.documents?.forEach { document ->
                val user = document.toObject(Users::class.java)
                val username = user?.username?.let { normalizeName(it) }
                if (friendList != null) {
                    if (user!!.userId != Utils.getUidLoggedIn() && user.userId in friendList && username?.contains(
                            normalizeName(searchQuery)
                        ) == true
                    ) {
                        user.let {
                            it.relation = FRIEND
                            usersList.add(it)
                        }
                    }
                }
                users.value = usersList
            }
        }
        return users
    }


}