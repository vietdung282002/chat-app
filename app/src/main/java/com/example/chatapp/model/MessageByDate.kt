package com.example.chatapp.model

data class MessageByDate(
    val date: String,
    val value: List<Messages>
)
