package com.example.chatapp.model

data class Messages(
    val fromId: String = "",
    val toId: String = "",
    val message: String = "",
    val type: String = "",
    val time: String = "",
    val imageUrl: ArrayList<String> = arrayListOf()
)