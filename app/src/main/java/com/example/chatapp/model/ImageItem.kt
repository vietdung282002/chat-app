package com.example.chatapp.model

data class ImageItem(
    val path: String,
    var isSelected: Boolean = false,
    var places: Int = 0
)
