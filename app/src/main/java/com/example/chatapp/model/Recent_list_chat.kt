package com.example.chatapp.model
data class RecentChat(
    val userId: String = "",
    val time: String = "",
    val fromId: String = "",
    val message: String = "",
    val unreadNumber: Int = 0,
)

data class PairChatUser(
    val recentChat: RecentChat = RecentChat(),
    var users: Users? = null
)