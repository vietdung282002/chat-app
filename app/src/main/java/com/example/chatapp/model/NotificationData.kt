package com.example.chatapp.model

data class NotificationData(
    val id: String,
    val name: String,
    val message: String,
    val type: String
)

data class PushNotification(val data: NotificationData, val to: String)

data class Token(val token: String? = "")