package com.example.chatapp.model

data class Section(
    val key: Char,
    val value: List<Users>
)
