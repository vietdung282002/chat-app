package com.example.chatapp.model

import com.example.chatapp.utils.Utils.Companion.CURRENT

data class Users (
    val userId: String = "",
    val imageUrl: String? = "",
    val username: String = "",
    val userEmail: String = "",
    var relation: Int = CURRENT,
    val birthday: String = "",
    val phone: String = ""
)