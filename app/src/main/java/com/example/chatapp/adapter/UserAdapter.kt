package com.example.chatapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.daimajia.swipe.SwipeLayout
import com.example.chatapp.R
import com.example.chatapp.databinding.UserItemBinding
import com.example.chatapp.model.Users
import com.example.chatapp.onClickInterface.OnUserClickListener
import com.example.chatapp.utils.Utils.Companion.FRIEND
import com.example.chatapp.utils.Utils.Companion.FRIEND_REQUEST
import com.example.chatapp.utils.Utils.Companion.NOT_RELATION
import de.hdodenhof.circleimageview.CircleImageView

class UserAdapter(private val context: Context) :
    ListAdapter<Users, UserAdapter.UserViewHolder>(UserItemDiffUtils()) {

    private var listener: OnUserClickListener? = null

    inner class UserViewHolder(userItemBinding: UserItemBinding) :
        RecyclerView.ViewHolder(userItemBinding.root) {
        private val tvUsername: TextView = userItemBinding.tvUsername
        private val imageViewUser: CircleImageView = userItemBinding.imageViewUser
        private val swipeLayout: SwipeLayout = userItemBinding.swipe
        private val denyBtn: TextView = userItemBinding.tvDeny
        private val acceptButton: Button = userItemBinding.acceptFriendBtn
        private val addFriendButton: Button = userItemBinding.addFriendBtn
        private val cancelButton: Button = userItemBinding.cancelFriendRequestBtn

        init {
            swipeLayout.addDrag(
                SwipeLayout.DragEdge.Right,
                itemView.findViewById(R.id.bottom_wrapper)
            )
        }

        fun bind(user: Users, position: Int) {
            tvUsername.text = user.username
            swipeLayout.showMode = SwipeLayout.ShowMode.PullOut
            if(user.imageUrl != ""){
                Glide.with(context).load(user.imageUrl).dontAnimate().into(imageViewUser)
            }

            when(user.relation){
                NOT_RELATION -> {
                    denyBtn.visibility = View.GONE
                    acceptButton.visibility = View.GONE
                    addFriendButton.visibility = View.VISIBLE
                    addFriendButton.setOnClickListener {
                        listener?.onAddFriendClicked(position,user)
                    }
                    cancelButton.visibility = View.GONE
                }

                FRIEND -> {
                    denyBtn.visibility = View.GONE
                    acceptButton.visibility = View.GONE
                    addFriendButton.visibility = View.GONE
                    cancelButton.visibility = View.GONE
                }

                FRIEND_REQUEST -> {
                    denyBtn.visibility = View.VISIBLE
                    denyBtn.setOnClickListener {
                        listener?.onDenyClicked(position,user)
                    }
                    acceptButton.visibility = View.VISIBLE
                    acceptButton.setOnClickListener {
                        listener?.onAcceptFriendClicked(position,user)
                    }
                    addFriendButton.visibility = View.GONE
                    cancelButton.visibility = View.GONE
                }

                else -> {
                    denyBtn.visibility = View.GONE
                    acceptButton.visibility = View.GONE
                    addFriendButton.visibility = View.GONE
                    cancelButton.visibility = View.VISIBLE
                    cancelButton.setOnClickListener {
                        listener?.onCancelClicked(position, user)
                    }
                }

            }
        }
    }

    class UserItemDiffUtils : DiffUtil.ItemCallback<Users>() {
        override fun areItemsTheSame(oldItem: Users, newItem: Users): Boolean {

            return oldItem.userId == newItem.userId
        }

        override fun areContentsTheSame(oldItem: Users, newItem: Users): Boolean {

            return oldItem == newItem
        }

    }

    fun setOnUserClickListener(listener: OnUserClickListener) {
        this.listener = listener
    }

    override fun submitList(list: List<Users>?) {
        super.submitList(list?.let { ArrayList(it) })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(
            UserItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }


    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val user = getItem(position)

        holder.bind(user,position)

        holder.itemView.setOnClickListener{
            listener?.onUserSelected(position,user)
        }
    }

}


