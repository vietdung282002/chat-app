package com.example.chatapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.chatapp.databinding.ChatItemByDayBinding
import com.example.chatapp.model.MessageByDate
import com.example.chatapp.model.Users

class MessagesByDateAdapter :
    ListAdapter<MessageByDate, MessagesByDateAdapter.MessagesByDateViewHolder>(
        MessagesByDateDiffUtil()
    ) {
    private lateinit var messagesAdapter: MessageAdapter
    private lateinit var user: Users

    fun setUser(newUser: Users) {
        user = newUser
    }

    class MessagesByDateViewHolder(chatItemByDayBinding: ChatItemByDayBinding) :
        RecyclerView.ViewHolder(chatItemByDayBinding.root) {
        val tvDate: TextView = chatItemByDayBinding.tvDate
        val rvMessages: RecyclerView = chatItemByDayBinding.rvMessagesRecyclerView
    }

    class MessagesByDateDiffUtil : DiffUtil.ItemCallback<MessageByDate>() {
        override fun areItemsTheSame(oldItem: MessageByDate, newItem: MessageByDate): Boolean {
            return oldItem.date == newItem.date
        }

        override fun areContentsTheSame(oldItem: MessageByDate, newItem: MessageByDate): Boolean {
            return oldItem == newItem
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessagesByDateViewHolder {
        return MessagesByDateViewHolder(
            ChatItemByDayBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: MessagesByDateViewHolder, position: Int) {
        val message = getItem(position)
        messagesAdapter = MessageAdapter()
        val layoutManager = LinearLayoutManager(holder.itemView.context)
        layoutManager.reverseLayout = true
        holder.rvMessages.layoutManager = layoutManager
        holder.rvMessages.adapter = messagesAdapter
        holder.tvDate.text = message.date
        messagesAdapter.submitList(message.value)
        messagesAdapter.setUser(user)
    }
}