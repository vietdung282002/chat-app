package com.example.chatapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.chatapp.databinding.HeaderItemBinding
import com.example.chatapp.model.Section
import com.example.chatapp.onClickInterface.OnUserClickListener

class SectionAdapter(private val onUserClickListener: OnUserClickListener) :
    ListAdapter<Section, SectionAdapter.SectionViewHolder>(SectionItemDiffUtil()) {
    private lateinit var userAdapter: UserAdapter

    class SectionViewHolder(headerItemBinding: HeaderItemBinding) :
        ViewHolder(headerItemBinding.root) {
        val tvSection: TextView = headerItemBinding.tvHeader
        val rvSection: RecyclerView = headerItemBinding.rvListUser
    }

    class SectionItemDiffUtil : DiffUtil.ItemCallback<Section>() {
        override fun areItemsTheSame(oldItem: Section, newItem: Section): Boolean {
            return oldItem.key == newItem.key
        }

        override fun areContentsTheSame(oldItem: Section, newItem: Section): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SectionViewHolder {
        return SectionViewHolder(
            HeaderItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: SectionViewHolder, position: Int) {
        val section = getItem(position)
        userAdapter = UserAdapter(holder.itemView.context)
        userAdapter.setOnUserClickListener(onUserClickListener)

        holder.tvSection.text = section.key.toString()
        holder.rvSection.adapter = userAdapter
        userAdapter.submitList(section.value)
    }


}

