package com.example.chatapp.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.chatapp.databinding.ImageItemBinding
import com.example.chatapp.model.ImageItem
import com.example.chatapp.onClickInterface.OnImageClicked

class ImageAdapter : ListAdapter<ImageItem, ImageAdapter.ImageViewHolder>(ImageItemDiffUtil()) {

    private var listener: OnImageClicked? = null

    class ImageViewHolder(imageItemBinding: ImageItemBinding) :
        RecyclerView.ViewHolder(imageItemBinding.root) {
        val imageView: ImageView = imageItemBinding.ivImageItem
        val numberTextView: TextView = imageItemBinding.tvNumberOfImage
    }

    class ImageItemDiffUtil : DiffUtil.ItemCallback<ImageItem>() {
        override fun areItemsTheSame(oldItem: ImageItem, newItem: ImageItem): Boolean {
            return newItem.path == oldItem.path
        }

        override fun areContentsTheSame(oldItem: ImageItem, newItem: ImageItem): Boolean {
            return newItem == oldItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val view = ImageItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ImageViewHolder(view)
    }

    override fun submitList(list: List<ImageItem>?) {
        super.submitList(list?.let { ArrayList(it) })
    }


    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val imageItem = getItem(position)
        if (imageItem != null) {
            holder.imageView.setImageURI(Uri.parse(imageItem.path))
            if (imageItem.isSelected) {
                holder.numberTextView.visibility = View.VISIBLE
                holder.numberTextView.text = imageItem.places.toString()
            } else {
                holder.numberTextView.visibility = View.GONE
                holder.numberTextView.text = ""
            }
        }
        holder.itemView.setOnClickListener {
            if (imageItem != null) {
                listener?.getOnRecentImageClicked(position, imageItem)
            }
        }

    }

    fun setOnImageClickListener(listener: OnImageClicked) {
        this.listener = listener
    }

}


