package com.example.chatapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.example.chatapp.databinding.ChatItemLeftBinding
import com.example.chatapp.databinding.ChatItemRightBinding
import com.example.chatapp.model.Messages
import com.example.chatapp.model.Users
import com.example.chatapp.utils.Utils

class MessageAdapter : ListAdapter<Messages, ViewHolder>(MessagesDiffUtil()) {
    private lateinit var user: Users

    fun setUser(newUser: Users) {
        user = newUser
    }

    private val left = 0
    private val right = 1
    private lateinit var imageMessageAdapter: ImageMessageAdapter

    class LeftViewHolder(chatItemLeftBinding: ChatItemLeftBinding) :
        ViewHolder(chatItemLeftBinding.root) {
        val messageText: TextView = chatItemLeftBinding.showMessage
        val timeSent: TextView = chatItemLeftBinding.timeView
        val imageUser: ImageView = chatItemLeftBinding.userImage
        val imageMessages: RecyclerView = chatItemLeftBinding.rvImageMessage
    }

    class RightViewHolder(chatItemRightBinding: ChatItemRightBinding) :
        ViewHolder(chatItemRightBinding.root) {
        val messageText: TextView = chatItemRightBinding.showMessage
        val timeSent: TextView = chatItemRightBinding.timeView
        val imageMessages: RecyclerView = chatItemRightBinding.rvImageMessage
    }

    class MessagesDiffUtil : DiffUtil.ItemCallback<Messages>() {
        override fun areItemsTheSame(oldItem: Messages, newItem: Messages): Boolean {
            return oldItem.time == newItem.time
        }

        override fun areContentsTheSame(oldItem: Messages, newItem: Messages): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return if (viewType == right) {
            val view =
                ChatItemRightBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            RightViewHolder(view)
        } else {
            val view =
                ChatItemLeftBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            LeftViewHolder(view)
        }
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val message = getItem(position)
        if (getItemViewType(position) == right) {
            val rightHolder = holder as RightViewHolder
            rightHolder.apply {
                timeSent.text = Utils.getTime(message.time)

                if (message.type == Utils.IMAGE && message.imageUrl.size > 0) {
                    imageMessages.visibility = View.VISIBLE
                    messageText.visibility = View.GONE
                    imageMessageAdapter = ImageMessageAdapter(message.imageUrl)
                    imageMessages.layoutManager =
                        GridLayoutManager(
                            itemView.context,
                            when (message.imageUrl.size) {
                                1 -> 1
                                2 -> 2
                                else -> 3
                            }
                        )
                    imageMessages.adapter = imageMessageAdapter
                    imageMessages.setOnClickListener {
                        if (timeSent.visibility == View.GONE) {
                            timeSent.visibility = View.VISIBLE
                        } else {
                            timeSent.visibility = View.GONE
                        }
                    }
                } else {
                    imageMessages.visibility = View.GONE
                    messageText.visibility = View.VISIBLE
                    messageText.text = message.message
                    messageText.setOnClickListener {
                        if (timeSent.visibility == View.GONE) {
                            timeSent.visibility = View.VISIBLE
                        } else {
                            timeSent.visibility = View.GONE
                        }
                    }
                }
            }
        } else {
            val leftViewHolder = holder as LeftViewHolder
            leftViewHolder.apply {
                if (user.imageUrl != null) {
                    Glide.with(itemView.context).load(user.imageUrl)
                        .into(holder.imageUser)
                }
                timeSent.text = Utils.getTime(message.time)
                if (message.type == Utils.IMAGE && message.imageUrl.size > 0) {
                    imageMessages.visibility = View.VISIBLE
                    messageText.visibility = View.GONE
                    imageMessageAdapter = ImageMessageAdapter(message.imageUrl)
                    imageMessages.layoutManager =
                        GridLayoutManager(
                            itemView.context,
                            when (message.imageUrl.size) {
                                1 -> 1
                                2 -> 2
                                else -> 3
                            }
                        )
                    imageMessages.adapter = imageMessageAdapter
                    imageMessages.setOnClickListener {
                        if (timeSent.visibility == View.GONE) {
                            timeSent.visibility = View.VISIBLE
                        } else {
                            timeSent.visibility = View.GONE
                        }
                    }
                } else {
                    imageMessages.visibility = View.GONE
                    messageText.visibility = View.VISIBLE
                    imageUser.visibility = View.VISIBLE

                    messageText.text = message.message

                    messageText.setOnClickListener {
                        if (timeSent.visibility == View.GONE) {
                            timeSent.visibility = View.VISIBLE
                        } else {
                            timeSent.visibility = View.GONE
                        }
                    }

                }
            }
        }
    }
    override fun getItemViewType(position: Int): Int =
        if (getItem(position).fromId == Utils.getUidLoggedIn()) right else left
}