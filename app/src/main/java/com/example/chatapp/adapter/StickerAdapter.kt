package com.example.chatapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.chatapp.databinding.StickerItemBinding
import com.example.chatapp.onClickInterface.OnStickerClicked

class StickerAdapter(private val stickerList: List<Int>) :
    RecyclerView.Adapter<StickerAdapter.StickerViewHolder>() {

    private var listener: OnStickerClicked? = null

    class StickerViewHolder(stickerItemBinding: StickerItemBinding) :
        RecyclerView.ViewHolder(stickerItemBinding.root) {
        val stickerImageView: ImageView = stickerItemBinding.ivStickerItem
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): StickerViewHolder {
        val view = StickerItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return StickerViewHolder(view)
    }

    override fun onBindViewHolder(holder: StickerViewHolder, position: Int) {
        val sticker = stickerList[position]
        holder.stickerImageView.setImageResource(sticker)
        holder.itemView.setOnClickListener {
            listener?.getOnRecentStickerClicked(position, sticker)
        }
    }

    override fun getItemCount(): Int {
        return stickerList.size
    }

    fun setOnStickerClickListener(listener: OnStickerClicked) {
        this.listener = listener
    }

}