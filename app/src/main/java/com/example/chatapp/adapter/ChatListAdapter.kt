package com.example.chatapp.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.chatapp.R
import com.example.chatapp.databinding.RecentListChatBinding
import com.example.chatapp.model.PairChatUser
import com.example.chatapp.onClickInterface.OnRecentChatClicked
import com.example.chatapp.utils.Utils
import de.hdodenhof.circleimageview.CircleImageView

@RequiresApi(Build.VERSION_CODES.O)

class ChatListAdapter :
    ListAdapter<PairChatUser, ChatListAdapter.ItemViewHolder>(RecentListChatDiffUtil()) {
    private var listener: OnRecentChatClicked? = null

    class ItemViewHolder(recentListChatBinding: RecentListChatBinding) :
        RecyclerView.ViewHolder(recentListChatBinding.root) {
        val circleImageView: CircleImageView = recentListChatBinding.imageViewUser
        val badgeLayout: FrameLayout = recentListChatBinding.layoutBadge
        val tvNumberUnread: TextView = recentListChatBinding.tvNumberUnread
        val tvUsername: TextView = recentListChatBinding.tvUsername
        val tvMessage: TextView = recentListChatBinding.tvMessage
        val tvTime: TextView = recentListChatBinding.tvTime

    }

    class RecentListChatDiffUtil : DiffUtil.ItemCallback<PairChatUser>() {
        override fun areItemsTheSame(oldItem: PairChatUser, newItem: PairChatUser): Boolean {
            return oldItem.users?.userId == newItem.users?.userId
        }

        override fun areContentsTheSame(oldItem: PairChatUser, newItem: PairChatUser): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout =
            RecentListChatBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(adapterLayout)
    }


    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = getItem(position)

        if (item.recentChat.fromId == Utils.getUidLoggedIn()) {
            val messageView = "You: " + item.recentChat.message
            holder.tvMessage.text = messageView
        } else {
            if (item.users != null) {
                val messageView =
                    Utils.extractLastName(item.users!!.username) + ": " + item.recentChat.message
                holder.tvMessage.text = messageView
            }
        }

        when (item.recentChat.unreadNumber) {

            0 -> {
                holder.badgeLayout.visibility = View.INVISIBLE
                val typeface = ResourcesCompat.getFont(holder.itemView.context, R.font.lato_light)
                holder.tvMessage.typeface = typeface
                holder.tvTime.typeface = typeface
            }

            in 1..9 -> {
                holder.badgeLayout.visibility = View.VISIBLE
                holder.tvNumberUnread.text = item.recentChat.unreadNumber.toString()
                val typeface = ResourcesCompat.getFont(holder.itemView.context, R.font.lato_bold)
                holder.tvMessage.typeface = typeface
                holder.tvTime.typeface = typeface
            }

            else -> {
                holder.badgeLayout.visibility = View.VISIBLE
                holder.tvNumberUnread.text = "+9"
                val typeface = ResourcesCompat.getFont(holder.itemView.context, R.font.lato_bold)
                holder.tvMessage.typeface = typeface
                holder.tvTime.typeface = typeface
            }
        }
        if (item.users != null) {
            holder.tvUsername.text = item.users!!.username
            Glide.with(holder.itemView.context).load(item.users!!.imageUrl).dontAnimate()
                .into(holder.circleImageView)
        }
        holder.tvTime.text = Utils.showTimestamp(holder.itemView.context, item.recentChat.time)

        holder.itemView.setOnClickListener {
            listener?.getOnRecentChatClicked(position, item)
        }
    }
    fun setOnRecentChatListener(listener: OnRecentChatClicked){
        this.listener = listener
    }

}
