package com.example.chatapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.chatapp.databinding.ImageMessageItemBinding


class ImageMessageAdapter(private val imageUrl: ArrayList<String>) :
    RecyclerView.Adapter<ImageMessageAdapter.ImageViewHolder>() {
    class ImageViewHolder(imageMessageItemBinding: ImageMessageItemBinding) :
        RecyclerView.ViewHolder(imageMessageItemBinding.root) {
        val ivImageItem: ImageView = imageMessageItemBinding.ivImageItem
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(
            ImageMessageItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return imageUrl.size
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val image = imageUrl[position]
        Glide.with(holder.itemView.context).load(image).into(holder.ivImageItem)
    }


}