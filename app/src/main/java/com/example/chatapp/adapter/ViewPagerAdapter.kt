package com.example.chatapp.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.chatapp.fragment.friendTab.AllFriendFragment
import com.example.chatapp.fragment.friendTab.AllUserFragment
import com.example.chatapp.fragment.friendTab.RequestFriendFragment

class ViewPagerAdapter(fragment: Fragment): FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return 3
    }

    override fun createFragment(position: Int): Fragment {
        return when(position){
            0 -> AllFriendFragment()
            1 -> AllUserFragment()
            else -> RequestFriendFragment()
        }
    }
}