@file:Suppress("DEPRECATED_IDENTITY_EQUALS", "DEPRECATION")

package com.example.chatapp.fragment

import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.commit
import com.bumptech.glide.Glide
import com.example.chatapp.R
import com.example.chatapp.databinding.FragmentEditProfileBinding
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.io.FileDescriptor
import java.io.IOException

@AndroidEntryPoint
class EditProfileFragment : Fragment() {
    private lateinit var fragmentEditProfileBinding: FragmentEditProfileBinding
    private val userViewModel: UserViewModel by activityViewModels()
    private var uri: Uri? = null
    private lateinit var fragmentManager: FragmentManager

    private var galleryActivityResultLauncher: ActivityResultLauncher<Intent> =
        registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) {
            if (it.resultCode === AppCompatActivity.RESULT_OK) {
                val imageBitmap = uriToBitmap(it.data?.data!!)
                fragmentEditProfileBinding.imageViewUser.setImageBitmap(imageBitmap)
                uploadImageToFirebaseStorage(imageBitmap)
            }
        }

    private var cameraActivityResultLauncher: ActivityResultLauncher<Intent> =
        registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) {
            if (it.resultCode === AppCompatActivity.RESULT_OK) {
                val imageBitmap = it.data?.extras?.get("data") as Bitmap
                fragmentEditProfileBinding.imageViewUser.setImageBitmap(imageBitmap)
                uploadImageToFirebaseStorage(imageBitmap)
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentEditProfileBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_edit_profile, container, false
        )
        return fragmentEditProfileBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragmentManager = requireActivity().supportFragmentManager



        fragmentEditProfileBinding.apply {


            viewModel = userViewModel
            lifecycleOwner = viewLifecycleOwner
            backBtn.setOnClickListener {
                fragmentManager.commit {
                    val fragment =
                        fragmentManager.findFragmentById(R.id.activity_fragment_container_view)
                    if (fragment != null) {
                        fragmentManager.beginTransaction().remove(fragment).commit()
                    }
                }
            }

            saveBtn.setOnClickListener {
                if (uri != null) {
                    userViewModel.updateProfile(
                        nameEditText.text.toString(),
                        phoneEditText.text.toString(),
                        birthdayEditText.text.toString(),
                        uri.toString()
                    )
                } else {
                    userViewModel.imageUrl.observe(viewLifecycleOwner) {
                        it?.let {
                            userViewModel.updateProfile(
                                nameEditText.text.toString(),
                                phoneEditText.text.toString(),
                                birthdayEditText.text.toString(),
                                it
                            )

                        }
                    }
                }
                fragmentManager.commit {
                    val fragment =
                        fragmentManager.findFragmentById(R.id.activity_fragment_container_view)
                    if (fragment != null) {
                        fragmentManager.beginTransaction().remove(fragment).commit()
                    }
                }
            }

            pickImageBtn.setOnClickListener {
                val options = arrayOf<CharSequence>(
                    getString(R.string.take_photo_button),
                    getString(R.string.choose_from_gallery_button),
                    getString(R.string.cancel_button)
                )
                val builder = AlertDialog.Builder(requireContext())
                builder.setTitle(getString(R.string.choose_picture_title))
                builder.setItems(options) { dialog, item ->
                    when {
                        options[item] == getString(R.string.take_photo_button) -> {
                            takePhotoWithCamera()
                        }

                        options[item] == getString(R.string.choose_from_gallery_button) -> {
                            pickImageFromGallery()
                        }

                        options[item] == getString(R.string.cancel_button) -> dialog.dismiss()
                    }
                }
                builder.show()
            }

        }

        userViewModel.imageUrl.observe(viewLifecycleOwner) {
            it?.let {
                Glide.with(this).load(it).dontAnimate()
                    .into(fragmentEditProfileBinding.imageViewUser)
            }
        }
    }

    private fun pickImageFromGallery() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        galleryActivityResultLauncher.launch(galleryIntent)
    }

    private fun takePhotoWithCamera() {

        if (checkSelfPermission(
                requireContext(),
                android.Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_DENIED
        ) {
            val permission = arrayOf(
                android.Manifest.permission.CAMERA
            )
            requestPermissions(permission, 112)
        } else {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            cameraActivityResultLauncher.launch(cameraIntent)
        }
    }

    private fun uriToBitmap(selectedFileUri: Uri): Bitmap? {
        try {
            val parcelFileDescriptor =
                activity?.contentResolver?.openFileDescriptor(selectedFileUri, "r")
            val fileDescriptor: FileDescriptor = parcelFileDescriptor!!.fileDescriptor
            val image = BitmapFactory.decodeFileDescriptor(fileDescriptor)
            parcelFileDescriptor.close()
            return image
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    private fun uploadImageToFirebaseStorage(imageBitmap: Bitmap?) {

        userViewModel.uploadImage(imageBitmap,
            onSuccess = {
                uri = it
            },
            onFailure = {
            }
        )

    }

}