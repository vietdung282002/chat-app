package com.example.chatapp.fragment.chatScreen

import android.app.Activity
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.chatapp.R
import com.example.chatapp.adapter.ImageAdapter
import com.example.chatapp.databinding.FragmentImageBinding
import com.example.chatapp.model.ImageItem
import com.example.chatapp.onClickInterface.OnImageClicked
import com.example.chatapp.utils.Utils
import com.example.chatapp.viewmodel.MessageViewModel
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ImageFragment : Fragment(), OnImageClicked {
    private lateinit var fragmentImageBinding: FragmentImageBinding
    private val messageViewModel: MessageViewModel by activityViewModels()
    private val userViewModel: UserViewModel by activityViewModels()

    private lateinit var userId: String
    private lateinit var imageList: ArrayList<ImageItem?>
    private lateinit var imageAdapter: ImageAdapter
    private var pickedImageItem: ArrayList<String> = arrayListOf()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentImageBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_image, container, false
        )
        return fragmentImageBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.onBackPressedDispatcher?.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    removeFragmentAndHideContainer()
                }
            })

        userId = arguments?.getString("userId").toString()


        imageList = getAllShownImagesPath(requireActivity())
        imageAdapter = ImageAdapter()
        imageAdapter.setOnImageClickListener(this)
        fragmentImageBinding.rvImagePicker.layoutManager = GridLayoutManager(requireContext(), 3)

        fragmentImageBinding.rvImagePicker.adapter = imageAdapter
        updateAdapter()
    }

    private fun updateAdapter() {
        imageAdapter.submitList(imageList.toList().map { it!!.copy() })
    }

    private fun getAllShownImagesPath(activity: Activity): ArrayList<ImageItem?> {
        val cursor: Cursor?
        val columnIndexData: Int
        val listOfAllImages = ArrayList<ImageItem?>()
        var absolutePathOfImage: String?
        val uri: Uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val projection = arrayOf(
            MediaStore.MediaColumns.DATA,
            MediaStore.Images.Media.BUCKET_DISPLAY_NAME
        )
        cursor = activity.contentResolver.query(
            uri, projection, null,
            null, "${MediaStore.Images.Media.DATE_TAKEN} DESC"
        )
        if (cursor != null) {
            columnIndexData = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
            while (cursor.moveToNext()) {
                absolutePathOfImage = cursor.getString(columnIndexData)
                listOfAllImages.add(
                    ImageItem(absolutePathOfImage, false)
                )
            }
            cursor.close()
        }
        return listOfAllImages
    }

    private fun sendImageMessage() {
        userViewModel.uploadMultipleImages(pickedImageItem,
            onSuccess = { imageUrls ->
                val message =
                    if (imageUrls.size == 1) "sent an image" else "sent " + imageUrls.size.toString() + " images"
                messageViewModel.sendImageMessage(
                    Utils.getUidLoggedIn(),
                    userId,
                    imageUrls,
                    message
                )
                pickedImageItem = arrayListOf()
                fragmentImageBinding.sendBtn.visibility = View.GONE
                updateList()
            },
            onFailure = {
            }
        )
//        for (item in pickedImageItem) {
//            val file = Uri.fromFile(File(item))
//            val storagePath = storageRef.child("Photos/${UUID.randomUUID()}.jpg")
//            val uploadTask = storagePath.putFile(file)
//            uploadTask.addOnFailureListener {}.addOnSuccessListener { taskSnapshot ->
//                taskSnapshot.storage.downloadUrl.addOnSuccessListener { uri ->
//                    imageUrls.add(uri.toString())
//                    if (imageUrls.size == pickedImageItem.size) {
//                        val message =
//                            if (imageUrls.size == 1) "sent an image" else "sent " + imageUrls.size.toString() + " images"
//                        messageViewModel.sendImageMessage(
//                            Utils.getUidLoggedIn(),
//                            userId,
//                            imageUrls,
//                            message
//                        )
//                        pickedImageItem = arrayListOf()
//                        fragmentImageBinding.sendBtn.visibility = View.GONE
//                        updateList()
//                    }
//                }
//            }
//        }

    }

    override fun getOnRecentImageClicked(position: Int, imageItem: ImageItem) {
        if (imageItem.isSelected) {
            pickedImageItem.remove(imageItem.path)
        } else {
            pickedImageItem.add(imageItem.path)
        }
        updateList()
        if (pickedImageItem.size > 0) {
            fragmentImageBinding.sendBtn.visibility = View.VISIBLE
            val text: String = "Send " + "(" + pickedImageItem.size.toString() + ")"
            fragmentImageBinding.sendBtn.text = text
            fragmentImageBinding.sendBtn.setOnClickListener {
                sendImageMessage()
            }
        } else {
            fragmentImageBinding.sendBtn.visibility = View.GONE
        }
    }

    private fun updateList() {
        for (item in imageList) {
            if (item != null) {
                val position = pickedImageItem.indexOf(item.path) + 1

                val isPicked = position != 0

                if (isPicked) {

                    item.isSelected = true
                    item.places = position
                    continue
                } else {
                    item.isSelected = false
                    item.places = 0
                }

            }
        }
        updateAdapter()
    }


    fun removeFragmentAndHideContainer() {
        if (isAdded) {
            val activity = requireActivity()
            val container = activity.findViewById<View>(R.id.image_fragment_container_view)
            parentFragmentManager.beginTransaction().remove(this).commit()
            container?.visibility = View.GONE
        }
    }

}