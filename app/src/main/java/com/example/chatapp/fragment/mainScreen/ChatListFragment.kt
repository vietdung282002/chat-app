package com.example.chatapp.fragment.mainScreen

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.chatapp.R
import com.example.chatapp.adapter.ChatListAdapter
import com.example.chatapp.databinding.FragmentChatListBinding
import com.example.chatapp.fragment.chatScreen.ChatFragment
import com.example.chatapp.model.PairChatUser
import com.example.chatapp.onClickInterface.OnRecentChatClicked
import com.example.chatapp.viewmodel.ChatListViewModel
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@RequiresApi(Build.VERSION_CODES.O)
@AndroidEntryPoint
class ChatListFragment : Fragment(), OnRecentChatClicked {
    private lateinit var fragmentChatListBinding: FragmentChatListBinding
    private lateinit var chatListAdapter: ChatListAdapter
    private val chatListViewModel: ChatListViewModel by activityViewModels()
    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var fragmentManager: FragmentManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentChatListBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_chat_list, container, false)
        return fragmentChatListBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragmentChatListBinding.lifecycleOwner = viewLifecycleOwner
        chatListAdapter = ChatListAdapter()
        chatListAdapter.setOnRecentChatListener(this)

        val layoutManager = LinearLayoutManager(context)
        fragmentChatListBinding.rvRecentListChat.layoutManager = layoutManager
        fragmentChatListBinding.rvRecentListChat.adapter = chatListAdapter

        chatListViewModel.getListChat().observe(viewLifecycleOwner) { list ->
            if (list != null) {
                val pairChatUser = list.map { PairChatUser(it.key, it.value) }.toMutableList()
                updateAdapter(pairChatUser)
            }
        }

        fragmentManager = requireActivity().supportFragmentManager


    }

    override fun getOnRecentChatClicked(position: Int, recentChatList: PairChatUser) {
        userViewModel.setSelectedUser(recentChatList.recentChat.userId)
        fragmentManager.commit {
            setReorderingAllowed(true)
            add<ChatFragment>(R.id.activity_fragment_container_view)
            addToBackStack(null)
        }
    }

    private fun updateAdapter(listChats: List<PairChatUser>) {
        chatListAdapter.submitList(listChats.toList().map { it.copy() })
    }
}