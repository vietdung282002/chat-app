package com.example.chatapp.fragment.friendTab

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.chatapp.R
import com.example.chatapp.adapter.ViewPagerAdapter
import com.example.chatapp.databinding.FragmentUserViewBinding
import com.example.chatapp.utils.Utils
import com.example.chatapp.viewmodel.UserViewModel
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserViewFragment : Fragment() {

    private lateinit var fragmentUserViewBinding: FragmentUserViewBinding
    private val userViewModel: UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentUserViewBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_user_view, container, false)
        return fragmentUserViewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewPagerAdapter = ViewPagerAdapter(this)

        fragmentUserViewBinding.viewPager.adapter = viewPagerAdapter
        val value = arguments?.getString(Utils.FRIEND_INTENT).toString()
        TabLayoutMediator(
            fragmentUserViewBinding.tabLayout,
            fragmentUserViewBinding.viewPager
        ) { tab, position ->
            when (position) {
                0 -> {
                    tab.text = getString(R.string.friend_tab_layout)
                }

                1 -> {
                    tab.text = getString(R.string.all_tab_layout)
                }

                else -> {
                    tab.text = getString(R.string.request_tab_layout)
                    userViewModel.friendRequestsNumber.observe(viewLifecycleOwner) {
                        val badge = tab.getOrCreateBadge()
                        badge.number = it
                        badge.isVisible = badge.number > 0
                    }
                }
            }

        }.attach()
        if (value == Utils.REQUEST_INTENT) {
            fragmentUserViewBinding.apply {
                viewPager.post {
                    viewPager.setCurrentItem(2, false)
                }
                tabLayout.post {
                    tabLayout.getTabAt(2)?.select()
                }
            }
        }


    }

}