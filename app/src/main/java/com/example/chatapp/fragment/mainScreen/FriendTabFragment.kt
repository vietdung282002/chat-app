package com.example.chatapp.fragment.mainScreen

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import com.example.chatapp.R
import com.example.chatapp.databinding.FragmentFriendTabBinding
import com.example.chatapp.fragment.SearchUserFragment
import com.example.chatapp.fragment.friendTab.UserViewFragment
import com.example.chatapp.utils.Utils
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FriendTabFragment : Fragment() {

    private lateinit var fragmentFriendTabBinding: FragmentFriendTabBinding
    private lateinit var fragmentManager: FragmentManager
    private val userViewModel: UserViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentFriendTabBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_friend_tab, container, false)
        return fragmentFriendTabBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragmentFriendTabBinding.lifecycleOwner = viewLifecycleOwner
        fragmentFriendTabBinding.viewModel = userViewModel
        fragmentManager = childFragmentManager

        fragmentFriendTabBinding.cancelBtn.setOnClickListener {
            hideKeyboard()
            fragmentFriendTabBinding.searchTextInput.apply {
                clearFocus()
                text?.clear()
            }
        }
        val value = arguments?.getString(Utils.FRIEND_INTENT).toString()
        val bundle = Bundle()

        fragmentFriendTabBinding.searchTextInput.onFocusChangeListener =
            View.OnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    fragmentFriendTabBinding.cancelBtn.visibility = View.VISIBLE
                    fragmentManager.commit {
                        setReorderingAllowed(true)
                        add<SearchUserFragment>(R.id.friend_fragment_container_view)
                        addToBackStack(null)
                    }
                } else {
                    fragmentFriendTabBinding.cancelBtn.visibility = View.GONE
                    val fragment =
                        fragmentManager.findFragmentById(R.id.friend_fragment_container_view)
                    if (fragment != null) {
                        fragmentManager.beginTransaction().remove(fragment).commit()
                    }
                }
            }

        val userViewFragment = UserViewFragment()
        bundle.putString(Utils.FRIEND_INTENT, value)
        userViewFragment.arguments = bundle

        fragmentManager.commit {
            setReorderingAllowed(true)
            add(R.id.friend_fragment_container_view, userViewFragment)
        }
    }

    private fun hideKeyboard() {
        val imm =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }
}