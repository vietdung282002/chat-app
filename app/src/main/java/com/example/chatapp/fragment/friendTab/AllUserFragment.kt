package com.example.chatapp.fragment.friendTab

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.example.chatapp.R
import com.example.chatapp.adapter.SectionAdapter
import com.example.chatapp.databinding.FragmentAllUserBinding
import com.example.chatapp.fragment.chatScreen.ChatFragment
import com.example.chatapp.model.Section
import com.example.chatapp.model.Users
import com.example.chatapp.onClickInterface.OnUserClickListener
import com.example.chatapp.utils.Utils
import com.example.chatapp.utils.Utils.Companion.FRIEND
import com.example.chatapp.utils.Utils.Companion.extractFirstLetter
import com.example.chatapp.utils.Utils.Companion.extractLastName
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.text.Collator

@AndroidEntryPoint
class AllUserFragment : Fragment(), OnUserClickListener {

    private lateinit var fragmentAllUserBinding: FragmentAllUserBinding
    private lateinit var sectionAdapter: SectionAdapter
    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var fragmentManager: FragmentManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentAllUserBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_all_user, container, false)
        return fragmentAllUserBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sectionAdapter = SectionAdapter(this)
        fragmentAllUserBinding.rvAllUser.adapter = sectionAdapter

        fragmentAllUserBinding.lifecycleOwner = viewLifecycleOwner
        userViewModel.mediatorLiveData.observe(viewLifecycleOwner) {
            userViewModel.getAllUser().observe(viewLifecycleOwner) {
                updateAdapter(it)
            }
        }

        fragmentManager = requireActivity().supportFragmentManager


    }

    private fun updateAdapter(listUser: List<Users>){
        val sortedList = listUser.sortedBy {
            val lastName = extractLastName(it.username)
            lastName.firstOrNull()?.uppercaseChar()
        }
        val sectionMap = sortedList.groupBy {
            val lastName = extractLastName(it.username)
            val firstLetter = extractFirstLetter(lastName)
            firstLetter
        }
        val vietnameseCollator = Collator.getInstance(java.util.Locale("vi"))
        val sections = sectionMap.entries.sortedBy { entry ->
            val collationKey = vietnameseCollator.getCollationKey(entry.key.toString())
            collationKey
        }
        val convertedList: List<Section> = sections.mapNotNull { entry ->
            val key = entry.key ?: return@mapNotNull null
            val value = entry.value
            Section(key, value)
        }

        sectionAdapter.submitList(convertedList)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onUserSelected(position: Int, users: Users) {
        if (users.relation == FRIEND) {
            userViewModel.setSelectedUser(users.userId)
            fragmentManager.commit {
                setReorderingAllowed(true)
                add<ChatFragment>(R.id.activity_fragment_container_view)
                addToBackStack(null)
            }
        }
    }

    override fun onAddFriendClicked(position: Int, users: Users) {
        userViewModel.addFriend(Utils.getUidLoggedIn(), users.userId)
    }

    override fun onAcceptFriendClicked(position: Int, users: Users) {
        userViewModel.acceptFriendRequest(users.userId, Utils.getUidLoggedIn())
    }

    override fun onCancelClicked(position: Int, users: Users) {
        userViewModel.removeRequestFriend(Utils.getUidLoggedIn(), users.userId)
    }

    override fun onDenyClicked(position: Int, users: Users) {
        userViewModel.denyFriendRequest(users.userId, Utils.getUidLoggedIn())
    }
}