@file:Suppress("DEPRECATION")

package com.example.chatapp.fragment.chatScreen

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.chatapp.R
import com.example.chatapp.adapter.MessagesByDateAdapter
import com.example.chatapp.databinding.FragmentChatBinding
import com.example.chatapp.model.MessageByDate
import com.example.chatapp.model.Messages
import com.example.chatapp.model.Users
import com.example.chatapp.utils.Utils
import com.example.chatapp.viewmodel.MessageViewModel
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChatFragment : Fragment() {
    private lateinit var fragmentChatBinding: FragmentChatBinding
    private val userViewModel: UserViewModel by activityViewModels()
    private val messageViewModel: MessageViewModel by viewModels()
    private lateinit var messagesByDateAdapter: MessagesByDateAdapter
    private var userId: String = ""
    private lateinit var fragmentManager: FragmentManager

    private val textWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable?) {
            updateSubmitButtonState()
        }
    }

    private fun updateSubmitButtonState() {
        fragmentChatBinding.apply {
            val message = editTextMessage.text.toString()

            if (message.isNotEmpty()) {
                sendBtn.setImageResource(R.drawable.sent_btn_active)
            } else {
                sendBtn.setImageResource(R.drawable.sent_btn_inactive)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentChatBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_chat, container, false
        )
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        return fragmentChatBinding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragmentManager = requireActivity().supportFragmentManager

        fragmentChatBinding.viewModel = userViewModel
        fragmentChatBinding.lifecycleOwner = this

        fragmentChatBinding.editTextMessage.addTextChangedListener(textWatcher)

        userViewModel.selectedUser.observe(viewLifecycleOwner) { it ->
            userId = it
            if (userId != "") {
                userViewModel.getUser(userId).observe(viewLifecycleOwner) { user ->
                    fragmentChatBinding.chatUserName.text = user.username
                    Glide.with(this).load(user.imageUrl).into(fragmentChatBinding.chatImageViewUser)
                    messageViewModel.getMessage(userId).observe(viewLifecycleOwner) {
                        if (it != null) {
                            updateAdapter(timeGroupedMap(it), user)
                            messageViewModel.markAsRead(user.userId)
                        }
                    }
                }
            }
        }


        fragmentChatBinding.sendBtn.setOnClickListener {
            if (fragmentChatBinding.editTextMessage.text.toString() != "") {
                messageViewModel.sendTextMessage(
                    Utils.getUidLoggedIn(),
                    userId,
                    fragmentChatBinding.editTextMessage
                )
            }
        }
        fragmentChatBinding.chatBackBtn.setOnClickListener {
            fragmentManager.commit {
                val fragment =
                    fragmentManager.findFragmentById(R.id.activity_fragment_container_view)
                if (fragment != null) {
                    fragmentManager.beginTransaction().remove(fragment).commit()
                }
            }
            activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
            userViewModel.setSelectedUser("")
        }

        fragmentChatBinding.apply {
            textInput.setEndIconOnClickListener {
                if (stickerFragmentContainerView.visibility == View.VISIBLE) {
                    closeStickerPicker()
                } else {
                    openStickerPicker()
                }

            }
            chooseImage.setOnClickListener {
                if (imageFragmentContainerView.visibility == View.VISIBLE) {
                    closeImagePicker()
                } else {
                    openImagePicker()
                }
            }
            editTextMessage.onFocusChangeListener =
                View.OnFocusChangeListener { _, hasFocus ->
                    if (hasFocus) {
                        if (stickerFragmentContainerView.visibility == View.VISIBLE) {
                            closeStickerPicker()
                        }
                        if (imageFragmentContainerView.visibility == View.VISIBLE) {
                            closeImagePicker()
                        }
                    } else {
                        hideKeyboard()
                    }
                }
        }


        messagesByDateAdapter = MessagesByDateAdapter()
        val layoutManager = LinearLayoutManager(requireContext())
        layoutManager.reverseLayout = true
        fragmentChatBinding.messagesByDateRecyclerView.layoutManager = layoutManager
        fragmentChatBinding.messagesByDateRecyclerView.adapter = messagesByDateAdapter
    }

    private fun updateAdapter(
        list: List<MessageByDate>,
        user: Users
    ) {
        messagesByDateAdapter.submitList(list)
        messagesByDateAdapter.setUser(user)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun timeGroupedMap(messages: List<Messages>): List<MessageByDate> {
        val timeGroupedMap = mutableMapOf<String, MutableList<Messages>>()

        for (message in messages) {
            val messageTime = Utils.getMessageDayKey(requireContext(), message.time)

            if (!timeGroupedMap.containsKey(messageTime)) {
                timeGroupedMap[messageTime] = mutableListOf()
            }
            timeGroupedMap[messageTime]?.add(message)
        }


        return timeGroupedMap.entries.map { entry ->
            val date = entry.key
            val value = entry.value
            MessageByDate(date, value)
        }
    }

    private fun openStickerPicker() {
        fragmentChatBinding.apply {
            imageFragmentContainerView.visibility = View.GONE
            val bundle = Bundle()
            bundle.putString("userId", userId)
            val fragment = StickerFragment()
            fragment.arguments = bundle
            fragmentManager.commit {
                setReorderingAllowed(true)
                replace(R.id.sticker_fragment_container_view, fragment)
                addToBackStack(null)
            }

            stickerFragmentContainerView.visibility = View.VISIBLE
            hideKeyboard()
            editTextMessage.clearFocus()
        }
    }

    private fun closeStickerPicker() {
        fragmentChatBinding.apply {
            stickerFragmentContainerView.visibility = View.GONE
            fragmentManager.popBackStack()
        }
    }

    private fun hideKeyboard() {
        val view = activity?.currentFocus
        if (view != null) {
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun openImagePicker() {
        val permission = if (Build.VERSION.SDK_INT >= 33) {
            arrayOf(
                android.Manifest.permission.READ_MEDIA_IMAGES,
            )
        } else {
            arrayOf(
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
            )
        }
        if (Build.VERSION.SDK_INT >= 33) {
            if (checkSelfPermission(
                    requireContext(),
                    android.Manifest.permission.READ_MEDIA_IMAGES
                ) == PackageManager.PERMISSION_DENIED
            ) {
                requestPermissions(permission, 112)
            } else {
                fragmentChatBinding.apply {
                    stickerFragmentContainerView.visibility = View.GONE
                    val bundle = Bundle()
                    bundle.putString("userId", userId)
                    val fragment = ImageFragment()
                    fragment.arguments = bundle
                    fragmentManager.commit {
                        setReorderingAllowed(true)
                        replace(R.id.image_fragment_container_view, fragment)
                        addToBackStack(null)
                    }
                    imageFragmentContainerView.visibility = View.VISIBLE
                    hideKeyboard()
                    editTextMessage.clearFocus()
                }
            }
        } else {
            if (checkSelfPermission(
                    requireContext(),
                    android.Manifest.permission.READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_DENIED
            ) {
                requestPermissions(permission, 112)
            } else {
                fragmentChatBinding.apply {
                    stickerFragmentContainerView.visibility = View.GONE
                    val bundle = Bundle()
                    bundle.putString("userId", userId)
                    val fragment = ImageFragment()
                    fragment.arguments = bundle
                    fragmentManager.commit {
                        setReorderingAllowed(true)
                        replace(R.id.image_fragment_container_view, fragment)
                        addToBackStack(null)
                    }
                    imageFragmentContainerView.visibility = View.VISIBLE
                    hideKeyboard()
                    editTextMessage.clearFocus()
                }
            }
        }

    }

    private fun closeImagePicker() {
        fragmentChatBinding.apply {
            imageFragmentContainerView.visibility = View.GONE
            fragmentManager.popBackStack()
        }
    }

}