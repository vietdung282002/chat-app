@file:Suppress("DEPRECATION")

package com.example.chatapp.fragment.mainScreen

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.bumptech.glide.Glide
import com.example.chatapp.R
import com.example.chatapp.activity.AuthenActivity
import com.example.chatapp.databinding.FragmentProfileBinding
import com.example.chatapp.fragment.EditProfileFragment
import com.example.chatapp.utils.MyApplication
import com.example.chatapp.utils.SharedPrefs
import com.example.chatapp.utils.Utils
import com.example.chatapp.viewmodel.UserViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import java.util.Locale

@AndroidEntryPoint
class ProfileFragment : Fragment() {
    private lateinit var fragmentProfileBinding: FragmentProfileBinding
    private lateinit var logOutPd: ProgressDialog
    private val userViewModel: UserViewModel by activityViewModels()
    private val mySharedPrefs = SharedPrefs(MyApplication.instance.applicationContext)
    private lateinit var fragmentManager: FragmentManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentProfileBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        return fragmentProfileBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragmentManager = requireActivity().supportFragmentManager

        logOutPd = ProgressDialog(activity)

        fragmentProfileBinding.logout.setOnClickListener {
            startActivity(Intent(activity, AuthenActivity::class.java))
            requireActivity().finish()
            userViewModel.signOut()
        }

        fragmentProfileBinding.laguage.setOnClickListener {
            val currentLocale = getCurrentLocale(requireContext()).language
            var checkedItem: Int
            val items =
                arrayOf(getString(R.string.vietnamese_label), getString(R.string.english_label))
            checkedItem = if (currentLocale == "vi") {
                0
            } else {
                1
            }
            MaterialAlertDialogBuilder(requireContext()).setTitle(context?.getString(R.string.language_label))
                .setPositiveButton(getString(R.string.ok_button)) { _, _ ->
                    when (checkedItem) {
                        0 -> {
                            if (currentLocale != "vi") {
                                changeLanguage("vi")

                            }
                        }

                        else -> {
                            if (currentLocale != "en") {
                                changeLanguage("en")

                            }
                        }
                    }
                }.setNegativeButton(getString(R.string.cancel_button)) { _, _ ->

                }.setSingleChoiceItems(items, checkedItem) { _, selectedItemIndex ->
                    checkedItem = selectedItemIndex
                }.setCancelable(true).show().setCanceledOnTouchOutside(true)
        }

        fragmentProfileBinding.tvVersion.hint = Utils.getAppVersion(requireContext())

        fragmentProfileBinding.viewModel = userViewModel
        fragmentProfileBinding.lifecycleOwner = viewLifecycleOwner

        userViewModel.imageUrl.observe(viewLifecycleOwner) {
            it?.let {
                Glide.with(this).load(it).dontAnimate().into(fragmentProfileBinding.imageViewUser)
                Glide.with(this).load(it).dontAnimate().into(fragmentProfileBinding.background)
            }
        }

        fragmentProfileBinding.editBtn.setOnClickListener {
            fragmentManager.commit {
                setReorderingAllowed(true)
                add<EditProfileFragment>(R.id.activity_fragment_container_view)
                addToBackStack(null)
            }
        }
    }

    private fun changeLanguage(languageCode: String) {
        val locale = Locale(languageCode)
        Locale.setDefault(locale)
        val configuration = Configuration()

        configuration.locale = locale

        resources.updateConfiguration(configuration, resources.displayMetrics)
        mySharedPrefs.setValue(Utils.LANGUAGE_PREFERENCES, languageCode)
        // Recreate the activity to apply language changes
        requireActivity().recreate()
    }

    private fun getCurrentLocale(context: Context): Locale {
        return context.resources.configuration.locales[0]

    }


}