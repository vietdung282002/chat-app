package com.example.chatapp.fragment.friendTab

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.add
import androidx.fragment.app.commit
import com.example.chatapp.R
import com.example.chatapp.adapter.SectionAdapter
import com.example.chatapp.databinding.FragmentAllFriendBinding
import com.example.chatapp.fragment.chatScreen.ChatFragment
import com.example.chatapp.model.Section
import com.example.chatapp.model.Users
import com.example.chatapp.onClickInterface.OnUserClickListener
import com.example.chatapp.utils.Utils
import com.example.chatapp.utils.Utils.Companion.extractLastName
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.text.Collator

@AndroidEntryPoint
class AllFriendFragment : Fragment(), OnUserClickListener {
    private lateinit var fragmentAllFriendBinding: FragmentAllFriendBinding
    private lateinit var sectionAdapter: SectionAdapter
    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var fragmentManager: FragmentManager


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentAllFriendBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_all_friend, container, false)
        return fragmentAllFriendBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragmentManager = requireActivity().supportFragmentManager

        fragmentAllFriendBinding.lifecycleOwner = viewLifecycleOwner

        sectionAdapter = SectionAdapter(this)
        fragmentAllFriendBinding.rvAllFriend.adapter = sectionAdapter

        userViewModel.friendLists.observe(viewLifecycleOwner) {
            userViewModel.getFriendUsers(it).observe(viewLifecycleOwner) { list ->
                updateAdapter(list)
            }
        }


    }
    private fun updateAdapter(listUser: List<Users>){
        val sortedList = listUser.sortedBy {
            val lastName = extractLastName(it.username)
            lastName.firstOrNull()?.uppercaseChar()
        }
        val sectionMap = sortedList.groupBy {
            val lastName = extractLastName(it.username)
            val firstLetter = lastName.let { lastName -> Utils.extractFirstLetter(lastName) }
            firstLetter
        }
        val vietnameseCollator = Collator.getInstance(java.util.Locale("vi"))
        val sections = sectionMap.entries.sortedBy { entry ->
            val collationKey = vietnameseCollator.getCollationKey(entry.key.toString())
            collationKey
        }
        val convertedList: List<Section> = sections.mapNotNull { entry ->
            val key = entry.key ?: return@mapNotNull null
            val value = entry.value
            Section(key, value)
        }
        sectionAdapter.submitList(convertedList)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onUserSelected(position: Int, users: Users) {
        userViewModel.setSelectedUser(users.userId)
        fragmentManager.commit {
            setReorderingAllowed(true)
            add<ChatFragment>(R.id.activity_fragment_container_view)
            addToBackStack(null)
        }
    }

    override fun onAddFriendClicked(position: Int, users: Users) {

    }

    override fun onAcceptFriendClicked(position: Int, users: Users) {
        
    }

    override fun onCancelClicked(position: Int, users: Users) {
        
    }

    override fun onDenyClicked(position: Int, users: Users) {
        
    }
}