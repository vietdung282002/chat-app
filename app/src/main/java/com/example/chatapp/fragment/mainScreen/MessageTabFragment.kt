package com.example.chatapp.fragment.mainScreen

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import com.example.chatapp.R
import com.example.chatapp.databinding.FragmentMessageTabBinding
import com.example.chatapp.fragment.CreateNewChatFragment
import com.example.chatapp.fragment.SearchUserFragment
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
@RequiresApi(Build.VERSION_CODES.O)
class MessageTabFragment : Fragment() {
    private lateinit var fragmentMessageTabBinding: FragmentMessageTabBinding
    private lateinit var fragmentManager: FragmentManager
    private val userViewModel: UserViewModel by viewModels()
    private lateinit var activityFragmentManager: FragmentManager


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentMessageTabBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_message_tab, container, false)
        return fragmentMessageTabBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        fragmentManager = childFragmentManager
        activityFragmentManager = requireActivity().supportFragmentManager


        fragmentMessageTabBinding.createNewMessageBtn.setOnClickListener {
            activityFragmentManager.commit {
                setReorderingAllowed(true)
                add<CreateNewChatFragment>(R.id.activity_fragment_container_view)
                addToBackStack(null)
            }
        }

        fragmentMessageTabBinding.cancelBtn.setOnClickListener {
            hideKeyboard()
            fragmentMessageTabBinding.searchTextInput.apply {
                clearFocus()
                text?.clear()
            }
        }

        fragmentMessageTabBinding.viewModel = userViewModel


        fragmentMessageTabBinding.searchTextInput.onFocusChangeListener =
            View.OnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    fragmentMessageTabBinding.cancelBtn.visibility = View.VISIBLE
                    fragmentManager.commit {
                        setReorderingAllowed(true)
                        add<SearchUserFragment>(R.id.message_fragment_container_view)
                        addToBackStack(null)
                    }
                } else {
                    hideKeyboard()
                    fragmentMessageTabBinding.cancelBtn.visibility = View.GONE
                    val fragment =
                        fragmentManager.findFragmentById(R.id.message_fragment_container_view)
                    if (fragment != null) {
                        fragmentManager.beginTransaction().remove(fragment).commit()
                    }
                }
            }
        fragmentMessageTabBinding.lifecycleOwner = viewLifecycleOwner
        fragmentManager.commit {
            setReorderingAllowed(true)
            add<ChatListFragment>(R.id.message_fragment_container_view)
        }
    }

    private fun hideKeyboard() {
        val imm =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }

}