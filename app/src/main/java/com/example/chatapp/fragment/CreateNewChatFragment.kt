package com.example.chatapp.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.example.chatapp.R
import com.example.chatapp.adapter.UserAdapter
import com.example.chatapp.databinding.FragmentCreateNewChatBinding
import com.example.chatapp.fragment.chatScreen.ChatFragment
import com.example.chatapp.model.Users
import com.example.chatapp.onClickInterface.OnUserClickListener
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CreateNewChatFragment : Fragment(), OnUserClickListener {
    private lateinit var fragmentCreateNewChatBinding: FragmentCreateNewChatBinding
    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var userAdapter: UserAdapter
    private lateinit var fragmentManager: FragmentManager


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentCreateNewChatBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_create_new_chat, container, false
        )
        return fragmentCreateNewChatBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragmentManager = requireActivity().supportFragmentManager

        fragmentCreateNewChatBinding.backBtn.setOnClickListener {
            fragmentManager.commit {
                val fragment =
                    fragmentManager.findFragmentById(R.id.activity_fragment_container_view)
                if (fragment != null) {
                    fragmentManager.beginTransaction().remove(fragment).commit()
                }
            }
        }

        fragmentCreateNewChatBinding.viewModel = userViewModel

        userAdapter = UserAdapter(requireContext())
        userAdapter.setOnUserClickListener(this)
        fragmentCreateNewChatBinding.rvSearchResult.adapter = userAdapter

        userViewModel.friendLists.observe(viewLifecycleOwner) {
            userViewModel.getFriendUsers(it).observe(viewLifecycleOwner) { list ->
                updateAdapter(list)
            }
        }
    }

    private fun updateAdapter(list: List<Users>?) {
        userAdapter.submitList(list)
    }

    override fun onUserSelected(position: Int, users: Users) {
        userViewModel.setSelectedUser(users.userId)
        fragmentManager.commit {
            setReorderingAllowed(true)
            replace<ChatFragment>(R.id.activity_fragment_container_view)
        }
    }

    override fun onAddFriendClicked(position: Int, users: Users) {
    }

    override fun onAcceptFriendClicked(position: Int, users: Users) {
    }

    override fun onCancelClicked(position: Int, users: Users) {
    }

    override fun onDenyClicked(position: Int, users: Users) {
    }
}