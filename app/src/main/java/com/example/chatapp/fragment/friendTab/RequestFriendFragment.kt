package com.example.chatapp.fragment.friendTab

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.chatapp.R
import com.example.chatapp.adapter.UserAdapter
import com.example.chatapp.databinding.FragmentRequestFriendBinding
import com.example.chatapp.model.Users
import com.example.chatapp.onClickInterface.OnUserClickListener
import com.example.chatapp.utils.Utils
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RequestFriendFragment : Fragment() , OnUserClickListener {
    private lateinit var fragmentRequestFriendBinding: FragmentRequestFriendBinding
    private lateinit var friendRequestAdapter: UserAdapter
    private lateinit var requestedFriendAdapter: UserAdapter
    private val userViewModel: UserViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentRequestFriendBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_request_friend, container, false)
        return fragmentRequestFriendBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragmentRequestFriendBinding.lifecycleOwner = viewLifecycleOwner

        friendRequestAdapter = UserAdapter(requireContext())
        friendRequestAdapter.setOnUserClickListener(this)
        fragmentRequestFriendBinding.rvFriendRequest.adapter = friendRequestAdapter

        userViewModel.friendRequests.observe(viewLifecycleOwner) {
            userViewModel.getFriendRequestsUsers(it).observe(viewLifecycleOwner) { list ->
                friendRequestAdapter.submitList(list)
            }
        }

        requestedFriendAdapter = UserAdapter(requireContext())
        requestedFriendAdapter.setOnUserClickListener(this)
        fragmentRequestFriendBinding.rvRequestedFriend.adapter = requestedFriendAdapter

        userViewModel.requestedFriend.observe(viewLifecycleOwner) {
            userViewModel.getRequestedFriendUsers(it).observe(viewLifecycleOwner) { list ->
                requestedFriendAdapter.submitList(list)
            }
        }

    }

    override fun onUserSelected(position: Int, users: Users) {

    }

    override fun onAddFriendClicked(position: Int, users: Users) {

    }

    override fun onAcceptFriendClicked(position: Int, users: Users) {
        userViewModel.acceptFriendRequest(users.userId, Utils.getUidLoggedIn())
    }

    override fun onCancelClicked(position: Int, users: Users) {
        userViewModel.removeRequestFriend(Utils.getUidLoggedIn(), users.userId)
    }

    override fun onDenyClicked(position: Int, users: Users) {
        userViewModel.denyFriendRequest(users.userId, Utils.getUidLoggedIn())
    }
}