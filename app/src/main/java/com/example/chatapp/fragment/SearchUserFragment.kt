package com.example.chatapp.fragment

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import com.example.chatapp.R
import com.example.chatapp.adapter.UserAdapter
import com.example.chatapp.databinding.FragmentSearchUserBinding
import com.example.chatapp.fragment.chatScreen.ChatFragment
import com.example.chatapp.model.Users
import com.example.chatapp.onClickInterface.OnUserClickListener
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchUserFragment : Fragment(), OnUserClickListener {

    private lateinit var fragmentSearchUserBinding: FragmentSearchUserBinding
    private val userViewModel: UserViewModel by viewModels(
        ownerProducer = { requireParentFragment() }
    )
    private val activityUserViewModel: UserViewModel by activityViewModels()
    private lateinit var userAdapter: UserAdapter
    private lateinit var fragmentManager: FragmentManager


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentSearchUserBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_search_user, container, false)
        return fragmentSearchUserBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragmentSearchUserBinding.lifecycleOwner = viewLifecycleOwner
        fragmentSearchUserBinding.viewModel = userViewModel

        userAdapter = UserAdapter(requireContext())
        userAdapter.setOnUserClickListener(this)
        fragmentSearchUserBinding.rvSearchResult.adapter = userAdapter



        userViewModel.searchQuery.observe(viewLifecycleOwner) { query ->
            if (query != "") {
                userViewModel.getSearchUser(query).observe(viewLifecycleOwner) {
                    if (it.isEmpty()) {
                        fragmentSearchUserBinding.noResult.visibility = View.VISIBLE
                        fragmentSearchUserBinding.result.visibility = View.GONE
                    } else {
                        fragmentSearchUserBinding.noResult.visibility = View.GONE
                        fragmentSearchUserBinding.result.visibility = View.VISIBLE
                        updateAdapter(it)
                    }
                }
            } else {
                fragmentSearchUserBinding.result.visibility = View.GONE
                fragmentSearchUserBinding.noResult.visibility = View.GONE
            }
        }
        fragmentManager = requireActivity().supportFragmentManager

    }

    private fun updateAdapter(listUser: List<Users>){
        userAdapter.submitList(listUser)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onUserSelected(position: Int, users: Users) {
        activityUserViewModel.setSelectedUser(users.userId)
        fragmentManager.commit {
            setReorderingAllowed(true)
            add<ChatFragment>(R.id.activity_fragment_container_view)
            addToBackStack(null)
        }
    }

    override fun onAddFriendClicked(position: Int, users: Users) {

    }

    override fun onAcceptFriendClicked(position: Int, users: Users) {

    }

    override fun onCancelClicked(position: Int, users: Users) {

    }

    override fun onDenyClicked(position: Int, users: Users) {

    }
}