package com.example.chatapp.fragment.chatScreen

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.content.res.AppCompatResources
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.chatapp.R
import com.example.chatapp.adapter.StickerAdapter
import com.example.chatapp.databinding.FragmentStickerBinding
import com.example.chatapp.onClickInterface.OnStickerClicked
import com.example.chatapp.utils.Utils
import com.example.chatapp.viewmodel.MessageViewModel
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StickerFragment : Fragment(), OnStickerClicked {
    private lateinit var fragmentStickerBinding: FragmentStickerBinding
    private lateinit var stickerAdapter: StickerAdapter
    private val messageViewModel: MessageViewModel by activityViewModels()
    private val userViewModel: UserViewModel by activityViewModels()
    private lateinit var userId: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentStickerBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_sticker, container, false
        )
        return fragmentStickerBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.onBackPressedDispatcher?.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    removeFragmentAndHideContainer()
                }
            })


        val stickerList = listOf(
            R.drawable.sticker_0,
            R.drawable.sticker_1,
            R.drawable.sticker_2,
            R.drawable.sticker_3,
            R.drawable.sticker_4,
            R.drawable.sticker_5,
        )

        userId = arguments?.getString("userId").toString()


        stickerAdapter = StickerAdapter(stickerList)
        stickerAdapter.setOnStickerClickListener(this)

        fragmentStickerBinding.rvSticker.layoutManager = GridLayoutManager(requireContext(), 3)
        fragmentStickerBinding.rvSticker.adapter = stickerAdapter

    }


    private fun drawableToBitmap(context: Context, drawableId: Int): Bitmap {
        val drawable: Drawable? = AppCompatResources.getDrawable(context, drawableId)
        if (drawable is BitmapDrawable) {
            return drawable.bitmap
        }

        // Tạo một Bitmap mới và vẽ Drawable lên đó
        val bitmap = Bitmap.createBitmap(
            drawable?.intrinsicWidth ?: 1,
            drawable?.intrinsicHeight ?: 1,
            Bitmap.Config.ARGB_8888
        )
        val canvas = android.graphics.Canvas(bitmap)
        canvas.drawColor(Color.WHITE)
        drawable?.setBounds(0, 0, canvas.width, canvas.height)
        drawable?.draw(canvas)
        return bitmap
    }

    override fun getOnRecentStickerClicked(position: Int, drawableId: Int) {
        val imageBitmap = drawableToBitmap(requireContext(), drawableId)

        userViewModel.uploadImage(imageBitmap,
            onSuccess = {
                val imageUrl = arrayListOf(it.toString())
                messageViewModel.sendImageMessage(
                    Utils.getUidLoggedIn(),
                    userId,
                    imageUrl,
                    "sent a sticker"
                )
            },
            onFailure = {
            }
        )
    }

    fun removeFragmentAndHideContainer() {
        if (isAdded) {
            val activity = requireActivity()
            val container = activity.findViewById<View>(R.id.sticker_fragment_container_view)
            parentFragmentManager.beginTransaction().remove(this).commit()
            container?.visibility = View.GONE
        }
    }
}