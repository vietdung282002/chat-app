@file:Suppress("DEPRECATION")

package com.example.chatapp.fragment

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.example.chatapp.MainActivity
import com.example.chatapp.R
import com.example.chatapp.databinding.FragmentSignInBinding
import com.example.chatapp.utils.Utils.Companion.isPasswordValid
import com.example.chatapp.utils.Utils.Companion.isValidEmail
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignInFragment : Fragment() {
    private lateinit var fragmentSignInBinding: FragmentSignInBinding
    private lateinit var fragmentManager: FragmentManager
    private lateinit var signInPd: ProgressDialog
    private val userViewModel: UserViewModel by activityViewModels()


    private val textWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable?) {
            updateSubmitButtonState()
        }
    }

    private fun updateSubmitButtonState() {
        fragmentSignInBinding.apply {
            val email = emailEditText.text.toString()
            val password = passwordEditText.text.toString()

            signInBtn.isEnabled = email.isNotEmpty() && password.isNotEmpty()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentSignInBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in, container, false)
        return fragmentSignInBinding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragmentManager = requireActivity().supportFragmentManager

        fragmentSignInBinding.scrollView.viewTreeObserver.addOnGlobalLayoutListener {
            fragmentSignInBinding.scrollView.fullScroll(View.FOCUS_DOWN)
        }


        signInPd = ProgressDialog(activity)

        fragmentSignInBinding.apply {
            emailEditText.addTextChangedListener(textWatcher)
            passwordEditText.addTextChangedListener(textWatcher)
            signInBtn.setOnClickListener {
                if(!isValidEmail(emailEditText.text.toString())){
                    showAlertDialog(getString(R.string.invalid_email_error))
                }else if(!isPasswordValid(passwordEditText.text.toString())){
                    showAlertDialog(getString(R.string.invalid_password_error))
                }else{
                    signIn(emailEditText.text.toString(),passwordEditText.text.toString())
                }
            }
            forgotPasswordBtn.setOnClickListener {

            }

            goToSignup.setOnClickListener {
                fragmentManager.commit {
                    setReorderingAllowed(true)
                    replace<SignUpFragment>(R.id.fragment_container_view)
                    addToBackStack(null)
                }
            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun signIn(email: String, password: String) {
        signInPd.show()
        signInPd.setMessage(getString(R.string.signIn))

        userViewModel.signInWithEmailAndPassword(email, password,
            onSuccess = {
                signInPd.dismiss()
                startActivity(Intent(activity, MainActivity::class.java))
                requireActivity().finish()
            },
            onError = { errorMessage ->
                signInPd.dismiss()
                showAlertDialog(errorMessage)
            }
        )
    }

    private fun showAlertDialog(error: String) {
        val alertDialogBuilder = AlertDialog.Builder(activity)

        // Set the title and message
        val title = getString(R.string.error_title)
        val spannableTitle = SpannableString(title)
        spannableTitle.setSpan(
            ForegroundColorSpan(Color.RED),
            0,
            title.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        alertDialogBuilder.setTitle(spannableTitle)
        alertDialogBuilder.setMessage(error)

        alertDialogBuilder.setPositiveButton(getString(R.string.ok_button)) { dialog, _ ->
            dialog.dismiss()
        }

        // Create and show the alert dialog
        val alertDialog: AlertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }
}