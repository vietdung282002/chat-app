@file:Suppress("DEPRECATION")

package com.example.chatapp.fragment

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import com.example.chatapp.MainActivity
import com.example.chatapp.R
import com.example.chatapp.databinding.FragmentSignUpBinding
import com.example.chatapp.utils.Utils.Companion.isPasswordValid
import com.example.chatapp.utils.Utils.Companion.isValidEmail
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignUpFragment : Fragment() {
    private lateinit var fragmentSignUpBinding: FragmentSignUpBinding
    private lateinit var fragmentManager: FragmentManager
    private lateinit var signUpPd: ProgressDialog
    private val userViewModel: UserViewModel by activityViewModels()


    private val textWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable?) {
            updateSubmitButtonState()
        }
    }

    private fun updateSubmitButtonState() {
        fragmentSignUpBinding.apply {
            val name = nameEditText.text.toString()
            val email = emailEditText.text.toString()
            val password = passwordEditText.text.toString()
            val termsChecked = checkbox.isChecked

            fragmentSignUpBinding.signUpBtn.isEnabled = name.isNotEmpty() && email.isNotEmpty() && password.isNotEmpty() && termsChecked
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        fragmentSignUpBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_sign_up, container, false
        )
        return fragmentSignUpBinding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragmentSignUpBinding.scrollView.viewTreeObserver.addOnGlobalLayoutListener { // Scroll to the bottom when the layout changes
            fragmentSignUpBinding.scrollView.fullScroll(View.FOCUS_DOWN)
        }


        signUpPd = ProgressDialog(activity)

        fragmentManager = requireActivity().supportFragmentManager

        fragmentSignUpBinding.apply {
            checkbox.setOnCheckedChangeListener{_,_ ->
                updateSubmitButtonState()
            }
            emailEditText.addTextChangedListener(textWatcher)
            nameEditText.addTextChangedListener(textWatcher)
            passwordEditText.addTextChangedListener(textWatcher)

            goToSignIn.setOnClickListener {
                fragmentManager.popBackStack()
            }
            backBtn.setOnClickListener {
                fragmentManager.popBackStack()
            }
            signUpBtn.setOnClickListener {
                if(!isValidEmail(emailEditText.text.toString())){
                    showAlertDialog(getString(R.string.invalid_email_error))
                }else if(!isPasswordValid(passwordEditText.text.toString())){
                    showAlertDialog(getString(R.string.invalid_password_error))
                }else{
                    signUp(nameEditText.text.toString(),emailEditText.text.toString(),passwordEditText.text.toString())
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun signUp(username: String, email: String, password: String) {
        signUpPd.show()
        signUpPd.setMessage(getString(R.string.signing_up_label))

        userViewModel.signUpWithEmailAndPassword(email, password, username,
            onSuccess = {
                signUpPd.dismiss()
                startActivity(Intent(requireActivity(), MainActivity::class.java))
                requireActivity().finish()
            },
            onError = { errorMessage ->
                signUpPd.dismiss()
                showAlertDialog(errorMessage)
            }
        )
    }

    private fun showAlertDialog(error: String) {
        val alertDialogBuilder = AlertDialog.Builder(activity)

        // Set the title and message
        val title = getString(R.string.error_title)
        val spannableTitle = SpannableString(title)
        spannableTitle.setSpan(
            ForegroundColorSpan(Color.RED),
            0,
            title.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        alertDialogBuilder.setTitle(spannableTitle)
        alertDialogBuilder.setMessage(error)

        alertDialogBuilder.setPositiveButton(getString(R.string.ok_button)) { dialog, _ ->
            dialog.dismiss()
        }

        // Create and show the alert dialog
        val alertDialog: AlertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }


}