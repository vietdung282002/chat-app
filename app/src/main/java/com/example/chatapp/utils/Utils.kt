package com.example.chatapp.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.annotation.RequiresApi
import com.example.chatapp.R
import com.google.firebase.auth.FirebaseAuth
import java.text.Normalizer
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.Date
import java.util.Locale
import java.util.TimeZone

class Utils {
    companion object {
        private var auth = FirebaseAuth.getInstance()
        private var userid: String = ""

        const val CURRENT = -1
        const val NOT_RELATION = 0
        const val FRIEND = 1
        const val FRIEND_REQUEST = 2
        const val REQUESTED_FRIEND = 3

        const val TEXT = "text"
        const val IMAGE = "image"

        const val MESSAGE_NOTIFICATION = "1"
        const val FRIEND_REQUEST_NOTIFICATION = "2"
        const val FRIEND_ACCEPT_NOTIFICATION = "3"

        const val USERID_INTENT = "userid"
        const val FRIEND_INTENT = "friend"
        const val REQUEST_INTENT = "request"
        const val ACCEPT_INTENT = "accept"

        const val USERNAME_PREFERENCES = "username"
        const val LANGUAGE_PREFERENCES = "language"
        const val TOKEN_PREFERENCES = "token"

        private val emailRegex = Regex(
            "^\\s*([a-zA-Z0-9.+_\\-]+)@([a-zA-Z0-9.\\-]+)\\.\\w{2,6}\\s*$"
        )

        fun getUidLoggedIn(): String {
            if (auth.currentUser != null) {
                userid = auth.currentUser!!.uid
            }

            return userid
        }

        @SuppressLint("SimpleDateFormat")
        fun getTime(): String {
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val date = Date(System.currentTimeMillis())

            return formatter.format(date)
        }



        fun getTime(postTime: String):String{
            val dateFormatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            dateFormatter.timeZone = TimeZone.getTimeZone("Asia/Ho_Chi_Minh")

            val timeFormatter = SimpleDateFormat("HH:mm", Locale.getDefault())
            dateFormatter.timeZone = TimeZone.getTimeZone("Asia/Ho_Chi_Minh")

            val time = dateFormatter.parse(postTime)

            return time.let { timeFormatter.format(it!!) }
        }

        @RequiresApi(Build.VERSION_CODES.O)
        fun getMessageDayKey(context: Context,postTime: String): String {
            val dateFormatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            dateFormatter.timeZone = TimeZone.getTimeZone("Asia/Ho_Chi_Minh")

            val datesResultFormatter = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
            datesResultFormatter.timeZone = TimeZone.getTimeZone("Asia/Ho_Chi_Minh")

            val postDate = dateFormatter.parse(postTime)
            val localPostDateTime =
                LocalDateTime.ofInstant(postDate!!.toInstant(), ZoneId.systemDefault())
            val currentDate = LocalDate.now()

            return when {
                localPostDateTime.toLocalDate() == currentDate -> context.getString(R.string.today_timestamp)
                localPostDateTime.toLocalDate() == currentDate.minusDays(1) -> context.getString(R.string.yesterday_timestamp)
                else -> datesResultFormatter.format(postDate)
            }
        }

        @RequiresApi(Build.VERSION_CODES.O)
        fun showTimestamp(context: Context,postTime: String): String {
            val dateFormatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            dateFormatter.timeZone = TimeZone.getTimeZone("Asia/Ho_Chi_Minh")

            val datesResultFormatter = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
            datesResultFormatter.timeZone = TimeZone.getTimeZone("Asia/Ho_Chi_Minh")

            val timeResultFormatter = SimpleDateFormat("HH:mm", Locale.getDefault())
            timeResultFormatter.timeZone = TimeZone.getTimeZone("Asia/Ho_Chi_Minh")

            val postDate = dateFormatter.parse(postTime)
            val localPostDateTime =
                LocalDateTime.ofInstant(postDate!!.toInstant(), ZoneId.systemDefault())
            val currentDate = LocalDate.now()

            return when {
                localPostDateTime.toLocalDate() == currentDate -> timeResultFormatter.format(
                    postDate
                )

                localPostDateTime.toLocalDate() == currentDate.minusDays(1) -> context.getString(R.string.yesterday_timestamp)
                else -> datesResultFormatter.format(postDate)
            }
        }


        fun extractLastName(fullName: String): String {
            val words = fullName.trim().split(" ")
            return if (words.isNotEmpty()) {
                words.last()
            } else {
                ""
            }
        }

        fun extractFirstLetter(username: String): Char? {
            val normalizedUsername = Normalizer.normalize(username, Normalizer.Form.NFD)
            val firstChar = normalizedUsername.firstOrNull { it.isLetter() }
            return firstChar?.uppercaseChar()
        }

        fun normalizeName(input: String): String {
            var temp = Normalizer.normalize(input, Normalizer.Form.NFD)
            temp = temp.replace("[^\\p{ASCII}]".toRegex(), "")
            temp = temp.lowercase(Locale.ROOT)
            return temp
        }

        fun isValidEmail(email: String): Boolean {
            return emailRegex.matches(email)
        }

        fun isPasswordValid(password: String): Boolean {
            // Check password length
            if (password.length < 8) {
                return false
            }
            //Check for at least one lowercase letter
            val containsLowerCase = password.any { it.isLowerCase() }

            // Check for at least one capital letter
            val containsUpperCase = password.any { it.isUpperCase() }

            // Check has at least one digit
            val containsDigit = password.any { it.isDigit() }

            // Returns true if all conditions are true
            return containsLowerCase && containsUpperCase && containsDigit
        }

        fun getAppVersion(context: Context): String {
            return try {
                val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
                packageInfo.versionName
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
                "Unknown"
            }
        }

    }
}