package com.example.chatapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.chatapp.model.RecentChat
import com.example.chatapp.model.Users
import com.example.chatapp.repository.ChatListRepo
import com.example.chatapp.utils.Utils
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChatListViewModel @Inject constructor(private val chatListRepo: ChatListRepo) : ViewModel() {

    private val db = FirebaseFirestore.getInstance()
    val unreadChatNumber = MutableLiveData<Int>()

    init {
        getUnreadChatNumber()
    }

    private fun getUnreadChatNumber() = viewModelScope.launch(Dispatchers.IO) {
        db.collection("ChatRoom").document(Utils.getUidLoggedIn()).collection("userId")
            .whereGreaterThan("unreadNumber", 0)
            .addSnapshotListener {snapshot, exception ->
                if(exception != null){
                    return@addSnapshotListener
                }
                if (snapshot != null) {
                    unreadChatNumber.postValue(snapshot.size())
                }
            }
    }

    fun getListChat(): LiveData<LinkedHashMap<RecentChat, Users?>> {
        return chatListRepo.getAllChatList()
    }

}