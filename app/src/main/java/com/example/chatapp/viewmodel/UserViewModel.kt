package com.example.chatapp.viewmodel

import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.chatapp.model.NotificationData
import com.example.chatapp.model.PushNotification
import com.example.chatapp.model.Token
import com.example.chatapp.model.Users
import com.example.chatapp.network.RetrofitInstance
import com.example.chatapp.repository.UsersRepo
import com.example.chatapp.utils.MyApplication
import com.example.chatapp.utils.SharedPrefs
import com.example.chatapp.utils.Utils
import com.example.chatapp.utils.Utils.Companion.getUidLoggedIn
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.installations.FirebaseInstallations
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.UUID
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(private val usersRepo: UsersRepo) : ViewModel() {
    private val userId = MutableLiveData<String>()
    val username = MutableLiveData<String>()
    val imageUrl = MutableLiveData<String>()
    val userEmail = MutableLiveData<String>()
    val phone = MutableLiveData<String>()
    val birthday = MutableLiveData<String>()

    val friendLists = MutableLiveData<List<String>>()
    val requestedFriend = MutableLiveData<List<String>>()
    val friendRequests = MutableLiveData<List<String>>()

    val friendRequestsNumber = MutableLiveData<Int>()

    val searchQuery = MutableLiveData<String>()

    private val mySharedPrefs = SharedPrefs(MyApplication.instance.applicationContext)

    val mediatorLiveData: MediatorLiveData<Unit> = MediatorLiveData<Unit>().apply {
        addSource(friendLists) {
            value = Unit
        }
        addSource(requestedFriend) {
            value = Unit
        }
        addSource(friendRequests) {
            value = Unit
        }
    }

    val selectedUser = MutableLiveData<String>()

    fun setSelectedUser(userId: String) {
        selectedUser.postValue(userId)
    }

    private val db = FirebaseFirestore.getInstance()
    private val auth: FirebaseAuth = FirebaseAuth.getInstance()
    private val storageRef: StorageReference = FirebaseStorage.getInstance().reference


    init {
        userId.value = getUidLoggedIn()
        getCurrentUserInfor()
        getFriendLists()
        getRequestedFriendLists()
        getFriendRequestsLists()
    }

    fun getCurrentUser() = auth.currentUser

    fun signOut() = auth.signOut()

    fun signInWithEmailAndPassword(
        email: String,
        password: String,
        onSuccess: () -> Unit,
        onError: (String) -> Unit
    ) {
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
            if (it.isSuccessful) {
                onSuccess()
            }
        }.addOnFailureListener {
            val errorMessage = when (it) {
                is FirebaseAuthInvalidCredentialsException -> "Email or password is incorrect."
                else -> "Login failed."
            }
            onError(errorMessage)
        }
    }

    fun signUpWithEmailAndPassword(
        email: String,
        password: String,
        username: String,
        onSuccess: () -> Unit,
        onError: (String) -> Unit
    ) {
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { signUpTask ->
            if (signUpTask.isSuccessful) {
                val user = auth.currentUser
                val emptyArray = arrayListOf<String>()
                val userHashMap = hashMapOf(
                    "userId" to user!!.uid,
                    "username" to username,
                    "userEmail" to email,
                    "imageUrl" to "https://www.pngarts.com/files/3/Avatar-Free-PNG-Image.png",
                    "birthday" to "",
                    "phone" to "",
                )
                db.collection("Users").document(user.uid).set(userHashMap)
                    .addOnSuccessListener {
                        val friendHashMap = hashMapOf(
                            "userId" to emptyArray
                        )
                        db.collection("Friend").document(user.uid).set(friendHashMap)
                            .addOnSuccessListener {
                                val friendRequestHashMap = hashMapOf(
                                    "requestedFriend" to emptyArray,
                                    "friendRequests" to emptyArray
                                )
                                db.collection("FriendRequest").document(user.uid)
                                    .set(friendRequestHashMap)
                                    .addOnSuccessListener {
                                        onSuccess()
                                    }
                                    .addOnFailureListener { onError("Failed to create friend request document.") }
                            }
                            .addOnFailureListener { onError("Failed to create friend document.") }
                    }
                    .addOnFailureListener { onError("Failed to create user document.") }
            } else {
                onError("Sign up failed.")
            }
        }.addOnFailureListener { onError("Sign up failed.") }
    }


    private fun getCurrentUserInfor() = viewModelScope.launch(Dispatchers.IO) {
        db.collection("Users").document(getUidLoggedIn()).addSnapshotListener { value, _ ->
            if (value!!.exists()) {
                val user = value.toObject(Users::class.java)
                if (user != null) {
                    username.value = user.username
                    imageUrl.value = user.imageUrl!!
                    userEmail.value = user.userEmail
                    phone.value = user.phone
                    birthday.value = user.birthday

                    mySharedPrefs.setValue(Utils.USERNAME_PREFERENCES, user.username)
                }
            }
        }
    }


    private fun getFriendLists() = viewModelScope.launch(Dispatchers.IO) {
        db.collection("Friend").document(getUidLoggedIn()).addSnapshotListener { snapshot, _ ->
            if (snapshot != null && snapshot.exists()) {
                val userData = snapshot.data
                val friendsList = userData?.get("userId") as List<*>?

                if (friendsList != null) {
                    val stringList = mutableListOf<String>()
                    for (friend in friendsList) {
                        friend.let { stringList.add(it.toString()) }
                    }
                    friendLists.postValue(stringList)
                }
            }
        }
    }

    private fun getFriendRequestsLists() {
        db.collection("FriendRequest").document(getUidLoggedIn()).addSnapshotListener { snapshot, _ ->
            if (snapshot != null && snapshot.exists()) {
                val userData = snapshot.data
                val friendRequestsLists = userData?.get("friendRequests") as List<*>?

                if (friendRequestsLists != null) {
                    friendRequestsNumber.postValue(friendRequestsLists.size)
                }

                if (friendRequestsLists != null) {
                    val stringList = mutableListOf<String>()
                    for (friend in friendRequestsLists) {
                        friend.let { stringList.add(it.toString()) }
                    }
                    friendRequests.postValue(stringList)
                }
            }
        }
    }

    private fun getRequestedFriendLists() {
        db.collection("FriendRequest").document(getUidLoggedIn()).addSnapshotListener { snapshot, _ ->
            if (snapshot != null && snapshot.exists()) {
                val userData = snapshot.data
                val friendRequestedLists = userData?.get("requestedFriend") as List<*>?

                if (friendRequestedLists != null) {
                    val stringList = mutableListOf<String>()
                    for (friend in friendRequestedLists) {
                        friend.let { stringList.add(it.toString()) }
                    }
                    requestedFriend.postValue(stringList)
                }
            }
        }
    }

    fun getUser(id: String): LiveData<Users> {
        return usersRepo.getUserById(id)
    }

    fun getAllUser():LiveData<List<Users>> {
        return usersRepo.getAllUser(friendLists.value,requestedFriend.value,friendRequests.value)
    }

    fun getFriendUsers(friendList: List<String>): LiveData<List<Users>> {
        return usersRepo.getFriendUsers(friendList)
    }

    fun getFriendRequestsUsers(friendRequests: List<String>): LiveData<List<Users>> {
        return usersRepo.getFriendRequestsUsers(friendRequests)
    }

    fun getRequestedFriendUsers(requestedFriend: List<String>): LiveData<List<Users>> {
        return usersRepo.getRequestedFriendUsers(requestedFriend)
    }

    fun getSearchUser(searchQuery: String): LiveData<List<Users>> {
        return usersRepo.getSearchUser(searchQuery, friendLists.value)
    }

    fun uploadImage(imageBitmap: Bitmap?, onSuccess: (Uri) -> Unit, onFailure: () -> Unit) {
        val baos = ByteArrayOutputStream()
        imageBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        val storagePath = storageRef.child("Photos/${UUID.randomUUID()}.jpg")
        val uploadTask = storagePath.putBytes(data)
        uploadTask.addOnSuccessListener { taskSnapshot ->
            taskSnapshot.metadata?.reference?.downloadUrl?.addOnSuccessListener { uri ->
                onSuccess(uri)
            } ?: onFailure()
        }.addOnFailureListener {
            onFailure()
        }
    }

    fun uploadMultipleImages(
        pickedImageItem: List<String>,
        onSuccess: (List<String>) -> Unit,
        onFailure: () -> Unit
    ) {
        val imageUrls = mutableListOf<String>()
        var uploadedCount = 0

        for (item in pickedImageItem) {
            val file = Uri.fromFile(File(item))
            val storagePath = storageRef.child("Photos/${UUID.randomUUID()}.jpg")
            val uploadTask = storagePath.putFile(file)

            uploadTask.addOnFailureListener {
                onFailure()
            }.addOnSuccessListener { taskSnapshot ->
                taskSnapshot.storage.downloadUrl.addOnSuccessListener { uri ->
                    imageUrls.add(uri.toString())
                    uploadedCount++
                    if (uploadedCount == pickedImageItem.size) {
                        onSuccess(imageUrls)
                    }
                }
            }
        }
    }


    fun updateProfile(username: String, phone: String, birthday: String, imageUrl: String) =
        viewModelScope.launch(Dispatchers.IO) {
            val hashMapUser = hashMapOf<String, Any>(
                "username" to username,
                "phone" to phone,
                "birthday" to birthday,
                "imageUrl" to imageUrl
            )

            db.collection("Users").document(getUidLoggedIn()).update(hashMapUser)
                .addOnCompleteListener {

                }
        }

    fun addFriend(sender: String, receiver: String) = viewModelScope.launch(Dispatchers.IO) {
        val senderUsername = mySharedPrefs.getValue(Utils.USERNAME_PREFERENCES)
        db.collection("FriendRequest").document(sender).update(
            hashMapOf<String, Any>("requestedFriend" to FieldValue.arrayUnion(receiver))
        )

        db.collection("FriendRequest").document(receiver).update(
            hashMapOf<String, Any>("friendRequests" to FieldValue.arrayUnion(sender))
        ).addOnSuccessListener {
            db.collection("Tokens").document(receiver).addSnapshotListener { value, _ ->


                if ((value != null) && value.exists() && (senderUsername != null)) {
                    val tokenObject = value.toObject(Token::class.java)

                    if (tokenObject != null) {
                        PushNotification(
                            NotificationData(
                                sender, senderUsername, "",
                                Utils.FRIEND_REQUEST_NOTIFICATION
                            ),
                            tokenObject.token!!
                        ).also {
                            sendNotification(it)
                        }
                    }
                }
            }

        }.addOnFailureListener { _ ->

        }
    }

    fun acceptFriendRequest(sender: String, receiver: String) = viewModelScope.launch(Dispatchers.IO) {
        val senderUsername = mySharedPrefs.getValue(Utils.USERNAME_PREFERENCES)

        db.collection("FriendRequest").document(sender).update(
            hashMapOf<String, Any>("requestedFriend" to FieldValue.arrayRemove(receiver))
        ).addOnSuccessListener {
            db.collection("Friend").document(sender).update(
                (
                        hashMapOf<String, Any>("userId" to FieldValue.arrayUnion(receiver))
                        )
            ).addOnSuccessListener {

            }
        }.addOnFailureListener { _ ->

        }

        db.collection("FriendRequest").document(receiver).update(
            hashMapOf<String, Any>("friendRequests" to FieldValue.arrayRemove(sender))
        ).addOnSuccessListener {
            db.collection("Friend").document(receiver).update((
                    hashMapOf<String, Any>("userId" to FieldValue.arrayUnion(sender))
                    )).addOnSuccessListener {
                db.collection("Tokens").document(sender).addSnapshotListener { value, _ ->


                    if ((value != null) && value.exists() && (senderUsername != null)) {
                        val tokenObject = value.toObject(Token::class.java)

                        if (tokenObject != null) {
                            PushNotification(
                                NotificationData(
                                    sender, senderUsername, "",
                                    Utils.FRIEND_ACCEPT_NOTIFICATION
                                ),
                                tokenObject.token!!
                            ).also {
                                sendNotification(it)
                            }
                        }
                    }
                }
            }
        }.addOnFailureListener { _ ->

        }
    }

    fun removeRequestFriend(sender: String, receiver: String) = viewModelScope.launch(Dispatchers.IO) {
        db.collection("FriendRequest").document(sender).update(
            hashMapOf<String, Any>("requestedFriend" to FieldValue.arrayRemove(receiver))
        ).addOnSuccessListener {

        }.addOnFailureListener { _ ->

        }

        db.collection("FriendRequest").document(receiver).update(
            hashMapOf<String, Any>("friendRequests" to FieldValue.arrayRemove(sender))
        ).addOnSuccessListener {

        }.addOnFailureListener { _ ->

        }
    }

    fun denyFriendRequest(sender: String, receiver: String) = viewModelScope.launch(Dispatchers.IO) {
        db.collection("FriendRequest").document(sender).update(
            hashMapOf<String, Any>("requestedFriend" to FieldValue.arrayRemove(receiver))
        ).addOnSuccessListener {

        }.addOnFailureListener { _ ->

        }

        db.collection("FriendRequest").document(receiver).update(
            hashMapOf<String, Any>("friendRequests" to FieldValue.arrayRemove(sender))
        ).addOnSuccessListener {

        }.addOnFailureListener { _ ->

        }
    }

    fun updateToken() {
        val firebaseInstance = FirebaseInstallations.getInstance()
        firebaseInstance.id.addOnSuccessListener { _ ->
            FirebaseMessaging.getInstance().token.addOnSuccessListener { getToken ->
                val hashMap = hashMapOf<String, Any>("token" to getToken)
                db.collection("Tokens").document(getUidLoggedIn()).set(hashMap)
                    .addOnSuccessListener {
                    }
                    .addOnFailureListener {
                    }
            }
        }.addOnFailureListener {
        }
    }

    private fun sendNotification(notification: PushNotification) = viewModelScope.launch {
        try {
            RetrofitInstance.api.postNotification(notification)
        } catch (_: Exception) {
        }
    }



}