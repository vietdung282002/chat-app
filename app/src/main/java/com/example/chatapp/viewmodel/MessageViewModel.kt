package com.example.chatapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.chatapp.model.Messages
import com.example.chatapp.model.NotificationData
import com.example.chatapp.model.PushNotification
import com.example.chatapp.model.Token
import com.example.chatapp.network.RetrofitInstance
import com.example.chatapp.repository.MessageRepo
import com.example.chatapp.utils.MyApplication
import com.example.chatapp.utils.SharedPrefs
import com.example.chatapp.utils.Utils
import com.example.chatapp.utils.Utils.Companion.MESSAGE_NOTIFICATION
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MessageViewModel @Inject constructor(private val messageRepo: MessageRepo) : ViewModel() {
    private val db = FirebaseFirestore.getInstance()
    private val mySharedPrefs = SharedPrefs(MyApplication.instance.applicationContext)
    private val senderUsername = mySharedPrefs.getValue(Utils.USERNAME_PREFERENCES)


    fun sendTextMessage(fromId: String, toId: String, messageInputText: TextInputEditText) =
        viewModelScope.launch(Dispatchers.IO) {
            val message = messageInputText.text.toString()
            val uniqueId = listOf(fromId, toId).sorted()
            uniqueId.joinToString(separator = "")


            val hashMap = hashMapOf<String, Any>(
                "fromId" to fromId,
                "toId" to toId,
                "message" to message,
                "time" to Utils.getTime(),
                "isSeen" to false,
                "imageUrl" to arrayListOf<String>(),
                "type" to Utils.TEXT
            )

            db.collection("Messages").document(uniqueId.toString()).collection("chats")
                .document(Utils.getTime()).set(hashMap).addOnCompleteListener {

                    val senderRef = db.collection("ChatRoom").document(fromId).collection("userId")
                        .document(toId)
                    senderRef.get().addOnSuccessListener {
                        val hashMapForSender = hashMapOf<String, Any>(
                            "userId" to toId,
                            "time" to Utils.getTime(),
                            "message" to message,
                            "fromId" to fromId,
                        )
                        if (it.exists()) {
                            senderRef.update(hashMapForSender).addOnSuccessListener {
                            }.addOnFailureListener {
                            }
                        } else {
                            senderRef.set(hashMapForSender)
                        }
                    }

                    val receiverRef = db.collection("ChatRoom").document(toId).collection("userId")
                        .document(fromId)

                    receiverRef.get().addOnSuccessListener {
                        val hashMapForReceiver = hashMapOf<String, Any>(
                            "userId" to fromId,
                            "time" to Utils.getTime(),
                            "message" to message,
                            "fromId" to fromId,
                        )
                        if (it.exists()) {
                            receiverRef.update(hashMapForReceiver)
                            receiverRef.update("unreadNumber", FieldValue.increment(1))
                        } else {
                            receiverRef.set(hashMapForReceiver)
                            receiverRef.update("unreadNumber", FieldValue.increment(1))
                        }
                    }

                    db.collection("Tokens").document(toId).addSnapshotListener { value, _ ->


                        if ((value != null) && value.exists() && (senderUsername != null)) {
                            val tokenObject = value.toObject(Token::class.java)

                            if (tokenObject != null) {
                                PushNotification(
                                    NotificationData(
                                        fromId,
                                        senderUsername,
                                        message,
                                        MESSAGE_NOTIFICATION
                                    ),
                                    tokenObject.token!!
                                ).also {
                                    sendNotification(it)
                                }
                            }
                        }
                    }

                    messageInputText.text?.clear()
                }.addOnFailureListener {

                }
        }

    private fun sendNotification(notification: PushNotification) = viewModelScope.launch {
        try {
            RetrofitInstance.api.postNotification(notification)
        } catch (_: Exception) {
        }
    }

    fun sendImageMessage(
        fromId: String,
        toId: String,
        imageUrl: List<String>,
        message: String
    ) =
        viewModelScope.launch(Dispatchers.IO) {

            val uniqueId = listOf(fromId, toId).sorted()
            uniqueId.joinToString(separator = "")
            val hashMap = hashMapOf<String, Any>(
                "fromId" to fromId,
                "toId" to toId,
                "message" to "",
                "time" to Utils.getTime(),
                "isSeen" to false,
                "imageUrl" to imageUrl,
                "type" to Utils.IMAGE
            )

            db.collection("Messages").document(uniqueId.toString()).collection("chats")
                .document(Utils.getTime()).set(hashMap).addOnCompleteListener {

                    val senderRef = db.collection("ChatRoom").document(fromId).collection("userId")
                        .document(toId)
                    senderRef.get().addOnSuccessListener {
                        val hashMapForSender = hashMapOf<String, Any>(
                            "userId" to toId,
                            "time" to Utils.getTime(),
                            "message" to message,
                            "fromId" to fromId,
                        )
                        if (it.exists()) {
                            senderRef.update(hashMapForSender).addOnSuccessListener {
                            }.addOnFailureListener {
                            }
                        } else {
                            senderRef.set(hashMapForSender)
                        }
                    }

                    val receiverRef = db.collection("ChatRoom").document(toId).collection("userId")
                        .document(fromId)

                    receiverRef.get().addOnSuccessListener {
                        val hashMapForReceiver = hashMapOf<String, Any>(
                            "userId" to fromId,
                            "time" to Utils.getTime(),
                            "message" to message,
                            "fromId" to fromId,
                        )
                        if (it.exists()) {
                            receiverRef.update(hashMapForReceiver)
                            receiverRef.update("unreadNumber", FieldValue.increment(1))
                        } else {
                            receiverRef.set(hashMapForReceiver)
                            receiverRef.update("unreadNumber", FieldValue.increment(1))
                        }
                    }

                    db.collection("Tokens").document(toId).addSnapshotListener { value, _ ->


                        if ((value != null) && value.exists() && (senderUsername != null)) {
                            val tokenObject = value.toObject(Token::class.java)

                            if (tokenObject != null) {
                                PushNotification(
                                    NotificationData(
                                        fromId,
                                        senderUsername,
                                        message,
                                        MESSAGE_NOTIFICATION
                                    ),
                                    tokenObject.token!!
                                ).also {
                                    sendNotification(it)
                                }
                            }
                        }
                    }

                }.addOnFailureListener {

                }
        }

    fun getMessage(userId: String): LiveData<List<Messages>> {
        return messageRepo.getMessage(userId)
    }

    fun markAsRead(userId: String) = viewModelScope.launch(Dispatchers.IO) {
        val uniqueId = listOf(Utils.getUidLoggedIn(), userId).sorted()
        uniqueId.joinToString(separator = "")
        val updates = hashMapOf<String, Any>(
            "isSeen" to true
            // Add more key-value pairs as needed
        )
        db.collection("Messages").document(uniqueId.toString()).collection("chats")
            .whereEqualTo("isSeen", false).whereEqualTo("fromId", userId).get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    db.collection("Messages").document(uniqueId.toString()).collection("chats")
                        .document(document.id).update(updates).addOnSuccessListener {

                        }
                }
            }
        db.collection("ChatRoom").document(Utils.getUidLoggedIn()).collection("userId")
            .document(userId).update("unreadNumber", 0)

    }


}