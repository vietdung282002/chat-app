package com.example.chatapp

import android.os.Build
import android.os.Bundle
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.example.chatapp.databinding.ActivityMainBinding
import com.example.chatapp.fragment.chatScreen.ChatFragment
import com.example.chatapp.fragment.mainScreen.FriendTabFragment
import com.example.chatapp.fragment.mainScreen.MessageTabFragment
import com.example.chatapp.fragment.mainScreen.ProfileFragment
import com.example.chatapp.utils.Utils
import com.example.chatapp.viewmodel.ChatListViewModel
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@RequiresApi(Build.VERSION_CODES.O)
@AndroidEntryPoint

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController
    private lateinit var binding: ActivityMainBinding
    private val userViewModel: UserViewModel by viewModels()
    private val chatListViewModel: ChatListViewModel by viewModels()
    private val messageTabFragment = MessageTabFragment()
    private val friendTabFragment = FriendTabFragment()
    private val profileTabFragment = ProfileFragment()
    private var activeFragment: Fragment = messageTabFragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.viewModel = userViewModel
        val navHostFrag =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFrag.navController

        userViewModel.mediatorLiveData.observe(this){

        }

        val messageBadge = binding.bottomNavigationView.getOrCreateBadge(R.id.messageTabFragment)
        val friendBadge = binding.bottomNavigationView.getOrCreateBadge(R.id.friendTabFragment)


        chatListViewModel.unreadChatNumber.observe(this) { unreadChatNumber ->
            messageBadge.number = unreadChatNumber
            messageBadge.isVisible = messageBadge.number > 0
        }

        userViewModel.friendRequestsNumber.observe(this) { friendRequestsNumber ->
            friendBadge.number = friendRequestsNumber
            friendBadge.isVisible = friendBadge.number > 0

        }
        val bundle = Bundle()
        if (intent.getStringExtra(Utils.FRIEND_INTENT) != null) {
            val value = intent.getStringExtra(Utils.FRIEND_INTENT)
            if (value == Utils.REQUEST_INTENT) {
                bundle.putString(Utils.FRIEND_INTENT, value)
            } else {
                bundle.putString(Utils.FRIEND_INTENT, value)
            }

            friendTabFragment.arguments = bundle

            supportFragmentManager.beginTransaction().apply {
                add(R.id.nav_host_fragment, messageTabFragment).hide(messageTabFragment)
                add(R.id.nav_host_fragment, friendTabFragment)
                add(R.id.nav_host_fragment, profileTabFragment).hide(profileTabFragment)
            }.commit()


        } else {
            supportFragmentManager.beginTransaction().apply {
                add(R.id.nav_host_fragment, messageTabFragment)
                add(R.id.nav_host_fragment, friendTabFragment).hide(friendTabFragment)
                add(R.id.nav_host_fragment, profileTabFragment).hide(profileTabFragment)
            }.commit()
        }



        binding.bottomNavigationView.setOnItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.messageTabFragment -> {
                    supportFragmentManager.beginTransaction().hide(activeFragment)
                        .show(messageTabFragment).commit()
                    activeFragment = messageTabFragment
                    true
                }

                R.id.friendTabFragment -> {
                    supportFragmentManager.beginTransaction().hide(activeFragment)
                        .show(friendTabFragment).commit()
                    activeFragment = friendTabFragment
                    true
                }

                R.id.profileFragment -> {
                    supportFragmentManager.beginTransaction().hide(activeFragment)
                        .show(profileTabFragment).commit()
                    activeFragment = profileTabFragment
                    true
                }

                else -> false
            }
        }




        generateToken()

        if (intent.getStringExtra(Utils.USERID_INTENT) != null) {
            val id = intent.getStringExtra(Utils.USERID_INTENT)
            if (id != null) {
                userViewModel.setSelectedUser(id)
            }
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                add<ChatFragment>(R.id.activity_fragment_container_view)
                addToBackStack(null)
            }
        }
    }


    private fun generateToken() {
        userViewModel.updateToken()
    }


}