@file:Suppress("DEPRECATION")

package com.example.chatapp.network

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Build
import android.text.Html
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.example.chatapp.MainActivity
import com.example.chatapp.R
import com.example.chatapp.utils.SharedPrefs
import com.example.chatapp.utils.Utils
import com.example.chatapp.utils.Utils.Companion.FRIEND_REQUEST_NOTIFICATION
import com.example.chatapp.utils.Utils.Companion.MESSAGE_NOTIFICATION
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlin.random.Random

private const val CHANNEL_ID = "my_channel"

class FirebaseService : FirebaseMessagingService() {
    companion object {

        private var sharedPrefs: SharedPreferences? = null
        var token: String?
            get() {
                return sharedPrefs?.getString(Utils.TOKEN_PREFERENCES, "")
            }
            set(value) {
                sharedPrefs?.edit()?.putString(Utils.TOKEN_PREFERENCES, value)?.apply()
            }

    }

    override fun onNewToken(newToken: String) {
        super.onNewToken(newToken)
        token = newToken
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)

        val intent = Intent(this, MainActivity::class.java)

        val type: String? = message.data["type"]

        when (type) {
            MESSAGE_NOTIFICATION -> {
                intent.putExtra(Utils.USERID_INTENT, message.data["id"])
            }

            FRIEND_REQUEST_NOTIFICATION -> {
                intent.putExtra(Utils.FRIEND_INTENT, Utils.REQUEST_INTENT)
            }

            else -> {
                intent.putExtra(Utils.FRIEND_INTENT, Utils.ACCEPT_INTENT)
            }
        }
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationID = Random.nextInt()

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            createNotificationChannel(notificationManager)
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this, 0, intent,
            PendingIntent.FLAG_CANCEL_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )


        val sharedCustomPref = SharedPrefs(applicationContext)
        sharedCustomPref.setIntValue("value", notificationID)

        val notification: Notification
        when (type) {
            MESSAGE_NOTIFICATION -> {
                val notificationContent =
                    "<b>${message.data["name"]}</b>:" + "${message.data["message"]}"
                notification = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentText(Html.fromHtml(notificationContent))
                    .setSmallIcon(R.drawable.noti_icon)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .build()
            }

            FRIEND_REQUEST_NOTIFICATION -> {
                val notificationContent =
                    "<b>${message.data["name"]}</b>:" + getString(R.string.sent_you_a_friend_request_content)
                notification = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentText(Html.fromHtml(notificationContent))
                    .setSmallIcon(R.drawable.noti_icon)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .build()
            }

            else -> {
                val notificationContent =
                    "<b>${message.data["name"]}</b>:" + getString(R.string.accept_your_friend_request_content)
                notification = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentText(Html.fromHtml(notificationContent))
                    .setSmallIcon(R.drawable.noti_icon)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .build()
            }
        }

        notificationManager.notify(notificationID, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(notificationManager: NotificationManager) {
        val channelName = "channelName"
        val channel = NotificationChannel(
            CHANNEL_ID,
            channelName,
            NotificationManager.IMPORTANCE_HIGH
        ).apply {
            description = "My channel description"
            enableLights(true)
            lightColor = Color.GREEN
        }
        notificationManager.createNotificationChannel(channel)
    }
}