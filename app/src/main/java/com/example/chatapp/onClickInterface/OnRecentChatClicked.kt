package com.example.chatapp.onClickInterface

import com.example.chatapp.model.PairChatUser

interface OnRecentChatClicked {
    fun getOnRecentChatClicked(position: Int, recentChatList: PairChatUser)
}