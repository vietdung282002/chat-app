package com.example.chatapp.onClickInterface

import com.example.chatapp.model.Users

interface OnUserClickListener {
    fun onUserSelected(position: Int,users: Users)

    fun onAddFriendClicked(position: Int, users: Users)

    fun onAcceptFriendClicked(position: Int, users: Users)

    fun onCancelClicked(position: Int, users: Users)

    fun onDenyClicked(position: Int, users: Users)

}