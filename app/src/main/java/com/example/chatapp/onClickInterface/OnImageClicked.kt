package com.example.chatapp.onClickInterface

import com.example.chatapp.model.ImageItem

interface OnImageClicked {
    fun getOnRecentImageClicked(position: Int, imageItem: ImageItem)
}