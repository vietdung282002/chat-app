@file:Suppress("DEPRECATION")

package com.example.chatapp.activity

import android.app.ProgressDialog
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.chatapp.MainActivity
import com.example.chatapp.R
import com.example.chatapp.databinding.ActivityAuthenBinding
import com.example.chatapp.utils.MyApplication
import com.example.chatapp.utils.SharedPrefs
import com.example.chatapp.utils.Utils
import com.example.chatapp.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.Locale

@AndroidEntryPoint
class AuthenActivity : AppCompatActivity() {
    private lateinit var activityAuthenBinding: ActivityAuthenBinding
    private lateinit var progressDialog: ProgressDialog
    private val userViewModel: UserViewModel by viewModels()
    private val mySharedPrefs = SharedPrefs(MyApplication.instance.applicationContext)

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityAuthenBinding = ActivityAuthenBinding.inflate(layoutInflater)
        setContentView(activityAuthenBinding.root)

        progressDialog = ProgressDialog(this)

        val savedLanguage = mySharedPrefs.getValue(Utils.LANGUAGE_PREFERENCES)
        if (!savedLanguage.isNullOrEmpty()) {
            changeLanguage(savedLanguage)
        }
        val user = userViewModel.getCurrentUser()
        if (user != null) {
            progressDialog.show()
            progressDialog.setMessage(getString(R.string.loading_text))
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        progressDialog.dismiss()

    }

    private fun changeLanguage(languageCode: String) {
        val locale = Locale(languageCode)
        Locale.setDefault(locale)
        val configuration = Configuration()

        configuration.locale = locale

        resources.updateConfiguration(configuration, resources.displayMetrics)
        mySharedPrefs.setValue(Utils.LANGUAGE_PREFERENCES, languageCode)
        // Recreate the activity to apply language changes
        this.recreate()
    }
}