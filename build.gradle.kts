  // Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    id("com.android.application") version "8.2.2" apply false
    id("org.jetbrains.kotlin.android") version "1.9.10" apply false
    id("com.google.gms.google-services") version "4.4.0" apply false
    id("com.google.dagger.hilt.android") version "2.51" apply false
}

  buildscript {
      repositories {
          google()
          mavenCentral()
      }
      dependencies {
          val navVersion = "2.7.7"
          classpath("androidx.navigation:navigation-safe-args-gradle-plugin:$navVersion")
          classpath("com.android.tools.build:gradle:8.2.2")
          classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.8.0")
          classpath("com.google.gms:google-services:4.4.1")
          classpath("com.google.dagger:hilt-android-gradle-plugin:2.51")
      }
  }
